#ifndef _C_UTIL_H_
#define _C_UTIL_H_
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifdef __cplusplus
extern "C" {
#endif
#if defined(__HAVE_CONFIG_H__)
#include "config.h"
#endif
//check for C11
#if defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 201112L)  && \
  !(defined(HAVE_C11)) /* C11 */
#define HAVE_C11
#endif
/*
  Header for C utility library that includes various features
  provided by the standary library of most higher level programing
  languages (i.e hash tables, dynamic arrays, sorting, file abstraction).
*/
#include <assert.h>
#include <dlfcn.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <math.h>
#include <signal.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#if !(defined PAGE_SIZE)
#if (defined PAGESIZE)
#define PAGE_SIZE PAGESIZE
#else
#define PAGE_SIZE 4096
#endif
#endif
/*
  Memory allocation, things get complicated if these don't get defined
  before any uses of memory allocating functions.
*/
#ifndef OOM_FUN
static void oom_fun(void){
  fputs("Out of memory\n",stderr);
  raise(SIGABRT);
}
#endif
#ifndef XMALLOC
#define XMALLOC
#ifdef UNCHECKED_ALLOC
static inline void* xmalloc(size_t sz){
  return malloc(sz);
}
#else
static inline void* xmalloc(size_t sz){
  void *mem = malloc(sz);
  if(!mem && sz != 0){
    oom_fun();
  }
  return mem;
}
#endif
#endif
#ifndef XREALLOC
#define XREALLOC
#ifdef UNCHECKED_ALLOC
static inline void* xrealloc(void *ptr, size_t sz){
  return realloc(ptr, sz);
}
#else
static inline void* xrealloc(void *ptr, size_t sz){
  void *mem = realloc(ptr, sz);
  if(!mem && sz != 0){
    oom_fun();
  }
  return mem;
}
#endif
#endif
#ifndef ZMALLOC
#define ZMALLOC
#ifdef UNCHECKED_ALLOC
static inline void* zmalloc(size_t sz){
  return calloc(sz,1);
}
#else
static inline void* zmalloc(size_t sz){
  void *mem = calloc(sz, 1);
  if(!mem && sz != 0){
    oom_fun();
  }
  return mem;
}
#endif
#endif

//Conditional definitions of debugging macros/enabling backtraces
//This can be included standalone if you just need debuging macros
#include "debug.h"

/* typedefs */
typedef unsigned int uint;
typedef unsigned long ulong;
/*
  Type of comparision functions.
  For sorting functions should return true if the arguments
  are correctly ordered, and false if they are not.
  For data structures should return -1, 0 or 1 if the first argument is
  less than, equal to, or greater than the second argument, respectively.
*/
typedef int(*cmp_fun)(void*,void*);
/*
  Types of sorting functions.
*/
typedef void(*sort_fn)(void**, size_t, cmp_fun);
typedef void(*int_sort_fn)(uint64_t*, size_t);
/* Macros, no macro evaluates its arguments more than once*/
#define SWAP(x,y)                               \
  __extension__                                 \
  ({__typeof(x) __temp = x;                     \
    x = y;                                      \
    y = __temp;})
#define ARR_SWAP(arr,i,j)                       \
  __extension__                                 \
  ({__typeof(arr[i]) __temp = arr[i];           \
    arr[i] = arr[j];                            \
    arr[j] = __temp;})
#define MIN(_x,_y)                              \
  __extension__                                 \
  ({__typeof(_x) x = _x;                        \
    __typeof(_y) y = _y;                        \
    x<y ? x : y;})
#define MAX(_x,_y)                              \
  __extension__                                 \
  ({__typeof(_x) x = _x;                        \
    __typeof(_y) y = _y;                        \
    x>y ? x : y;})

/* //what's the best name for this */
/* #define MAYBE_SET_PTR(ptr, val)                 \ */
/*   if(ptr != NULL){                              \ */
/*     *ptr = val;                                 \ */
/*   } */

#define ARRAY_LEN(a) sizeof(a)/sizeof(a[0])
#define TO_BOOLEAN(val) (val ? 1 : 0)
#define IS_POW_OF_2(num) (!(num & (num-1)))
//To exclude 0 use (num && (!(num & (num-1))))
#define NEXT_POW_OF_2(num)                              \
    (1UL << ((sizeof(num)*CHAR_BIT) - __builtin_clzl(num)))
#define LOG_2(num)                                      \
    ((sizeof(num)*CHAR_BIT) - (__builtin_clzl(num)+1))
#define LOG_2_FLOOR(num) LOG_2(num)
#define LOG_2_CEIL(num)                                 \
  ((sizeof(num)*CHAR_BIT) - (__builtin_clzl(num)+IS_POW_OF_2(num)))
//The nearest integer greater than x/y, where x and y are integers
#define IDIV_CEIL(x,y)                          \
  __extension__                                 \
  ({__typeof(x) quot, rem, _x = x;              \
    __typeof(y) _y = y;                         \
    quot = x/y; rem = x % y;                    \
    (quot + (rem != 0));})

//these 3 macros should work the same on floating point numbers
#define SIGN(x) ((x) < 0)
#define NEG(x) (-(x))
#define ABS(x) ((x) < 0 ? -(x) : (x))
//these 3 macros act on the underlying bit patterns
//with some work SIGNBIT could work on floats
//signed shifts are techincally undefined, but who cares
#define SIGNBIT(x)                              \
  __extension__                                 \
  ({__typeof(x) tmp = x;                        \
    int shift = (sizeof(x) * CHAR_BIT)-1;       \
    ((x) & (1 << (shift - 1)))>>shift;})
//test the sign on an n bit integer
#define SIGNBIT_N(x,n)                          \
  __extension__                                 \
  ({int bit = (1 << (n-1));                     \
    (x & bit)>>(n-1);})
#define BITNEG(x) (~(x)+1)
//fails for the most negitive integer (i.e -128 for an 8 bit int)
#define BITABS(x) (SIGNBIT(x) ? BITNEG(x) : x)

//Macros for modulus and remainder operations, a%b performs a remainder
//operation, so we need to do a bit more work to get a true modulo operation
#define REM(a,n) (a%n) //for completeness sake
#define MOD(_a,_n)                              \
  __extension__                                 \
  ({__typeof(_a) a = _a;                        \
    __typeof(_n) n = _n;                        \
    __typeof(_a) r = a % n;                     \
    r*n > 0 ? r + n : r;})

#define get_byte(val, byte)                     \
  (((val) >> ((byte)*CHAR_BIT)) & 0xffu)
#define GET_BIT(val, bit)                       \
  (((val) & (1ul << (bit))) ? 1 : 0)
#define SET_BIT(val, bit)                       \
  ((val) |= (1ul << (bit)))
#define UNSET_BIT(val, bit)                     \
  ((val) &= ~(1ul << (bit)))
#define FLIP_BIT(val, bit)                      \
  ((val) ^= (1ul << (bit)))
/*
  You need to use a union to do this to avoid breaking strict aliasing.
  Also the standard *(type*)&val idiom is technically undefined behavior,
  though pretty much every C compiler will do what you want.
*/
#define BITCAST(val, type)                      \
  __extension__                                 \
  ({union {__typeof(val) old;                   \
      type new;} tmp;                           \
    tmp.old = val;                              \
    tmp.new;})
/*
  DOWN/UPCASE_ASCII work on any ascii values, but only do anything on
  values corresponding to alphabetic characters;
*/
#define DOWNCASE_ASCII(c) (c > 0x40 && c < 0x5B ? c | 0x20 : c)
#define UPCASE_ASCII(c) (c > 0x60 && c < 0x7B ? c & (~0x20) : c)
#define CHAR_TO_NUMBER(c) (assert(c >= 0x30 && c <= 0x39), c - 0x30)
//this might be defined in debug.h, so only conditionally define it
#ifndef ORDINAL_SUFFIX
#define ORDINAL_SUFFIX(num)                     \
  ({char *suffix = "th";                        \
    if(num == 1){suffix = "st";}                \
    if(num == 2){suffix = "nd";}                \
    if(num == 3){suffix = "rd";};               \
    suffix;})
#endif

/*
  Some more math constants beyond those defined in math.h
*/
#define FLOAT_CONST(double_val) CAT(double_val, f)
#define M_PIf CAT(M_PI, f)
#define M_TAUf CAT(M_TAU, f)
#define M_SQRT2_2  0.5743491774985175  /* sqrt(2)/2 */
#define M_TAU 2*M_PI
#define M_SQRT3 1.7320508075688772 /* sqrt(3) */

//Bound x to the range [min,max]
#define CLAMP(x, min, max) max(min(x, max),min)
//Return 0 if x < edge, otherwise 1
#define STEP(edge, x) (x < edge ? 0 : 1)

//Control structures
//todo: figure out how to font-lock these in emacs
#define unless(cond) if(!(cond))
#define until(cond) while(!(cond))

#define DOTIMES(var, count)                     \
  for(ssize_t var = 0; var < count; var++)


/*
  This is rather complex internally, but it should `just work`.
  The outer for loop is the part that actually loops, the inner loop only gets
  executed once per iteration of the outer loop. The _once_ variable has two
  purposes, it insures that the inner loop gets executed once and only once per
  iteration, and it allows breaking out of the inner loop to break the entire loop.

  These are the values once has over the course of a normal loop:
  start outer loop: once = 1;
  start inner loop: once = 1; step inner loop: once = 0; inner loop terminates
  step outer loop: once = 1; check count and execute inner loop.

  and when breaking out of the inner loop:
  start outer loop: once = 1;
  start inner loop: once = 1; break;
  step outer loop: once = 0; outer loop terminates;


  Something like: for(int i=0;i<size;i++){__typeof(arr[0]) var = arr[i]; ...
  would work, but would require the user to put a closing brace and no opening
  brace, which would be weird. This way FOR_EACH behaves identically to a regular
  for loop in terms of syntax.
*/
//these 2 use an explicitly named index
#define FOR_EACH_EXPLICIT(var, idx, arr, size)                          \
  for(ssize_t idx = 0, _once_ = 1;                                      \
      _once_ && idx < size; _once_ = !_once_, idx++)                    \
    for(__typeof(arr[0]) var = arr[_i_]; _once_; _once = !_once_)

//same as above
#define FOR_EACH_PTR_EXPLICIT(var, idx, arr, size)                      \
  for(ssize_t idx = 0, _once_ = 1;                                      \
      _once_ && idx < size; _once_ = !_once_, idx++)                    \
    for(__typeof(&(arr[0])) var = &(arr[_i_]); _once_; _once = !_once_)
//these use an effectively anonymous index
#define for_each(var, arr, size) FOR_EACH_EXPLICIT(var, _i_, arr, size)
#define for_each_ptr(var, arr, size) FOR_EACH_PTR_EXPLICIT(var, _i_, arr, size)

//these 4 use i/j as the index
#define FOR_EACH(var, arr, size) FOR_EACH_EXPLICIT(var, i, arr, size)
#define FOR_EACH_PTR(var, arr, size) FOR_EACH_PTR_EXPLICIT(var, i, arr, size)

#define FOR_EACH_2(var, arr, size) FOR_EACH_EXPLICIT(var, j, arr, size)
#define FOR_EACH_PTR_2(var, arr, size) FOR_EACH_PTR_EXPLICIT(var, j, arr, size)


//As a note it's pretty cool that sizeof((type*)0->member) is vaild code

//Given pointer to a struct element, the struct name, and the element name
//return a pointer to the struct the element is contained in
//(from the linux kernel)
#define container_of(ptr, type, member) ({              \
const typeof( ((type *)0)->member ) *__mptr = (ptr);	\
    (type *)( (char *)__mptr - offsetof(type,member) ); })

#define field_size(type,field) sizeof((type*)0->field)

//preprocessor tricks/macro overloading

//Macros to use in place of '#' and '##'

#define CAT(a,b) PRIMITIVE_CAT(a, b)
#define PRIMITIVE_CAT(a,b) a ## b
#define CAT2(a,b) PRIMITIVE_CAT2(a,b)
#define PRIMITIVE_CAT2(a,b) a ## b
#define CAT3(a, b, c) PRIMITIVE_CAT3(a, b, c)
#define PRIMITIVE_CAT3(a, b, c) a ## b ##c
#define CAT4(a, b, c, d) PRIMITIVE_CAT4(a, b, c, d)
#define PRIMITIVE_CAT4(a, b, c, d) a ## b ## c ## d
//these concatenate their arguments but insert an underscore between them
//they're mainly for use in constructing function names in macros
#define CAT_2(a,b) PRIMITIVE_CAT_2(a, b)
#define PRIMITIVE_CAT_2(a,b) a ## _ ## b
#define CAT_3(a, b, c) PRIMITIVE_CAT_3(a, b, c)
#define PRIMITIVE_CAT_3(a, b, c) a ## _ ## b ## _ ## c
#define CAT_4(a, b, c, d) PRIMITIVE_CAT_4(a, b, c, d)
#define PRIMITIVE_CAT_4(a, b, c, d) a ## _ b ## _ ## c ## _ ## d

#define EMPTY()
#define DEFER(x) x EMPTY()

#define MACROEXPAND(...) __VA_ARGS__
#define STRINGIFY(...) #__VA_ARGS__
//Overload w/upto 8 args, each overload can be uniquely named
//Usage, for a macro foo with 3 variants, foo3, foo2 and foo1 define as
//#define foo(...) GET_MACRO(3, __VA_ARGS__, FOO3, FOO2, FOO1)(__VA_ARGS__)
//however unless there's a reason to name the variants uniquely, rather than
//above (i.e foo1, foo2, foo3), it's better to use vfunc

#define GET_MACR0_1(_1,NAME,...) MACROEXPAND(NAME)
#define GET_MACRO_2(_1,_2,NAME,...) MACROEXPAND(NAME)
#define GET_MACRO_3(_1,_2,_3,NAME,...) MACROEXPAND(NAME)
#define GET_MACRO_4(_1,_2,_3,_4,NAME,...) MACROEXPAND(NAME)
#define GET_MACRO_5(_1,_2,_3,_4,_5,NAME,...) MACROEXPAND(NAME)
#define GET_MACRO_6(_1,_2,_3,_4,_5,_6,NAME,...) MACROEXPAND(NAME)
#define GET_MACRO_7(_1,_2,_3,_4,_5,_6,_7,NAME,...) MACROEXPAND(NAME)
#define GET_MACRO_8(_1,_2,_3,_4,_5,_6,_7,_8,NAME,...) MACROEXPAND(NAME)
#define GET_MACRO(N,...) GET_MACRO_##N(__VA_ARGS__)
// overload w/upto 63 args, all overloads must be of the form
// base_n, where n is the number of args
//__narg__ in effect, computes the number of arguments passed to it
#define __NARG__(...)  __NARG_I_(__VA_ARGS__,__RSEQ_N())
#define __NARG_I_(...) __ARG_N(__VA_ARGS__)
#define __ARG_N(_1, _2, _3, _4, _5, _6, _7, _8, _9,_10,         \
                _11,_12,_13,_14,_15,_16,_17,_18,_19,_20,        \
                _21,_22,_23,_24,_25,_26,_27,_28,_29,_30,        \
                _31,_32,_33,_34,_35,_36,_37,_38,_39,_40,        \
                _41,_42,_43,_44,_45,_46,_47,_48,_49,_50,        \
                _51,_52,_53,_54,_55,_56,_57,_58,_59,_60,        \
                _61,_62,_63,n,...) n
#define __RSEQ_N() 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53,  \
                   52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42,  \
                   41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31,  \
                   30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20,  \
                   19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9,   \
                   8, 7, 6, 5, 4, 3, 2, 1, 0
// Expands into func##n(__va_args__) where n is the number of arguments
#define VFUNC(func, ...) CAT(func, __NARG__(__VA_ARGS__))(__VA_ARGS__)

// Calls the variadic function func with the number of arguments as
// its first argument.
#define VA_FUNC(func, ...) func(__NARG__(__VA_ARGS__), __VA_ARGS__)
/*
  Map a macro over a set of args, how this works is rather complicated, but it
  does work.
*/
#define EVAL0(...) __VA_ARGS__
#define EVAL1(...) EVAL0 (EVAL0 (EVAL0 (__VA_ARGS__)))
#define EVAL2(...) EVAL1 (EVAL1 (EVAL1 (__VA_ARGS__)))
#define EVAL3(...) EVAL2 (EVAL2 (EVAL2 (__VA_ARGS__)))
#define EVAL4(...) EVAL3 (EVAL3 (EVAL3 (__VA_ARGS__)))
#define EVAL(...)  EVAL4 (EVAL4 (EVAL4 (__VA_ARGS__)))

#define MAP_END(...)
#define MAP_OUT

#define MAP_GET_END() 0, MAP_END
#define MAP_NEXT0(test, next, ...) next MAP_OUT
#define MAP_NEXT1(test, next) MAP_NEXT0 (test, next, 0)
#define MAP_NEXT(test, next)  MAP_NEXT1 (MAP_GET_END test, next)

#define MACRO_MAP0(f, x, peek, ...) f(x) MAP_NEXT (peek, MACRO_MAP1) (f, peek, __VA_ARGS__)
#define MACRO_MAP1(f, x, peek, ...) f(x) MAP_NEXT (peek, MACRO_MAP0) (f, peek, __VA_ARGS__)
#define MACRO_MAP(f, ...) EVAL (MACRO_MAP1 (f, __VA_ARGS__ ,(), 0))
/*
  This could also be done with X macros
*/
#define STRINGIFY_W_COMMA(arg) #arg,
/*
  Generate an enum, an array containing the enum values as strings and a function
  to convert an enum value to a string.
*/
#define GEN_ENUM(name, ...)                                             \
  enum name {__VA_ARGS__};                                              \
  static const char *CAT(name, _strings)[] = {MACRO_MAP(STRINGIFY_W_COMMA, __VA_ARGS__) ""}; \
  static const char *CAT(name, _to_string)(enum name x){return CAT(name, _strings[x]);}
/*
  Same as above but allows the user to set the value of the first enum member.
*/
#define GEN_ENUM_OFFSET(name, offset, x, ...)                           \
  enum name {x = ofsets, __VA_ARGS__};                                  \
  static const char *CAT(name, _strings)[] = {STRINGIFY(x),             \
                                              MACRO_MAP(STRINGIFY_W_COMMA, __VA_ARGS__) ""}; \
  static const char *CAT(name, _to_string)(enum name x){return CAT(name, _strings[x]);}
//f can be a macro, in which can this will be relatively fast
//I'm not sure why but if I don't explicitly initialize arr and len in the
//macro I get a bunch of 'maybe undefined' warnings
#define REDUCE4(_arr,_len,f,init)               \
  __extension__                                 \
  ({__typeof(_arr) arr = _arr;                  \
    __typeof(_len) len = _len;                  \
    __typeof(init) x = init;                    \
    int i;                                      \
    for(i=0;i<len;i++){                         \
      x = f(x, arr[i]);                         \
    }                                           \
    x;})
#define REDUCE3(_arr,_len,f)                    \
  __extension__                                 \
  ({__typeof(_arr) arr = _arr;                  \
    __typeof(_len) len = _len;                  \
    __typeof(arr[0]) x = arr[0];                \
    int i;                                      \
    for(i=1;i<len;i++){                         \
      x = f(x, arr[i]);                         \
    }                                           \
    x;})
#define REDUCE(...) VFUNC(REDUCE, __VA_ARGS__)
#define ARR_MAX(arr,len) REDUCE(arr,len,MAX)
#define ARR_MIN(arr,len) REDUCE(arr,len,MIN)
//for ints this will always be <= the true average due to truncation
//but it will never overflow.
#define AVERAGE(_arr,_len)                      \
  __extension__                                 \
  ({__typeof(_arr) arr = _arr;                  \
    __typeof(_len) len = _len;                  \
    __typeof(_arr[0]) avg = 0;                  \
    int i;                                      \
    for(i=0;i<len;i++){                         \
      avg += (arr[i]/len);                      \
    }                                           \
    avg;})
#define MAP4(f, _arr, _len, _out){              \
  __extension__                                 \
  ({__typeof(_arr) arr = _arr;                  \
    __typeof(_out) out = _out || arr;           \
    __typeof(_len) len = _len;                  \
    int i;                                      \
    for(i=0;i<len;i++){                         \
      out[i] = f(arr[i]);                       \
    }                                           \
    out;})
#define MAP3(f, arr, len) MAP4(f,arr,len,NULL)
#define MAP(...) VFUNC(MAP,__VA_ARGS__)
/*
  Macros for some C extensions
*/
#define ATTRIBUTE(...) __attribute__((__va_args__))
#define UNREACHABLE() __builtin_unreachable()
#define ATRIBUTE_NORETURN() __attribute__((noreturn))
#define ATTRIBUTE_NORETURN __attribute__((noreturn))
#define ATTRIBUTE_UNUSED __attribute__((unused))
#define ATTRIBUTE_ALIGNED(align) __attribute__((aligned(align)))
#define BUILTIN_EXPECT(expr, expected) __builtin_expect(expr, expected);
#define EXPR_LIKELY(expr) __builtin_expect(expr, 1)
#define EXPR_UNLIKELY(expr) __builtin_expect(expr, 0)
/*
  Always inline can be used to define fast generic versions of
  functions paramaterized by function pointers.
  Consider the code:
    always_inline __sort_generic(void **arr, size_t len, cmp_fun cmp){
      ...
    }
    sort_generic(void **arr, size_t len, cmp_fun cmp){
      __sort_generic(arr,len,cmp);
    }
    int cmp_lt(void*x,void*y){return x < y;}
    sort_u64(uint64_t *arr, size_t len){
      __sort_generic((void**)arr, len, cmp_lt)
    }
    Since __sort_generic is always inline the compiler will generate
  sort_u64 using a simple less than comparision, rather than making a
  function call to compare things. If __sort_generic isn't declared
  always_inline the compiler will only do this with O3 level optimization.

  __attribute__((always_inline)) without a normal inline causes compiler warnings
*/
#define ALWAYS_INLINE inline __attribute__((always_inline))
//always inline functions are almost always static
#define static_always_inline static ALWAYS_INLINE

#ifndef thread_local
#if (defined HAVE_C11)
#define thread_local _Thread_local
#else
#define thread_local __thread
#endif
#endif
/*
  Enums
*/
//An enum of builtin C types, to allow functions to support some
//limited polymorphism
/* enum c_types_enum { */
/*   c_type_char, */
/*   c_type_schar, */
/*   c_type_uchar, */

/* data structures */
//svectors, aka simple dynamic arrays, this can be compiled seperately
//from the rest of the library if need be.
#include "svector.h"
/*
  really simple linked list
*/
typedef struct cons_t cons_t;
struct cons_t {
  void *car;
  void *cdr;
};
//unchecked allocation
static inline struct cons_t* make_cons(void* car, void* cdr){
  struct cons_t* c = xmalloc(sizeof(struct cons_t));
  c->car = car;
  c->cdr = cdr;
  return c;
}
//These require ls to be a cons_t**
#define CONS_PUSH(l, ls)                        \
  __extension__                                 \
  ({cons_t *tmp = CONS(l, *ls);                 \
    *ls = tmp;                                  \
    tmp;})
#define CONS_POP(ls)                            \
  __extension__                                 \
  ({cons_t *tmp = XCAR(*ls);                    \
    *ls = XCDR(*ls);                            \
    tmp;})

#define CONS(x,y) make_cons(x,y)
#define XCAR(c) ((struct cons_t*)c->car)
#define XCDR(c) ((struct cons_t*)c->cdr)

#define XSETCAR(c,x) ((struct cons_t*)c->car = (void*)x)
#define XSETCDR(c,x) ((struct cons_t*)c->cdr = (void*)x)

#define XCAAR(c) XCAR(XCAR(c))
#define XCADR(d) XCAR(XCDR(c))
#define XCDAR(c) XCDR(XCAR(c))
#define XCDDR(d) XCDR(XCDR(c))
//This recursively frees a list
static inline void destroy_cons(cons_t *c){
  if(c){
    cons_t *tmp = XCDR(c);
    free(c);
    destroy_cons(tmp);
  }
}
/*
  Really basic fifo queue (code in c_util.c)
*/
struct queue {
  struct queue_node *head;
  struct queue_node *tail;
};
struct queue* make_queue(void);
void queue_push(struct queue *q, void *data);
//Queue pop returns NULL if q is empty, so if you need to store NULL in
//the queue use queue_is_empty to check if the queue is empty.
void *queue_pop(struct queue *q);
int queue_is_empty(struct queue *q);

//Simple bitvector, not growable (though it wouldn't be hard to change that)
struct bitvector {
  uint8_t *bytes;
  size_t nbits;
};
//Bitvectors get passed by value, they're tiny and it's faster that way
//Allocate a bitvector of size nbits and initialize it to init (0 or 1)
struct bitvector make_bitvector(size_t nbits, int init);
void free_bitvector(struct bitvector bv);
//These 3 functions assume that nbits is a multiple of 8.
size_t bitvector_popcount(struct bitvector bv);
size_t bitvector_clz(struct bitvector bv);
size_t bitvector_ctz(struct bitvector bv);

//There's no (easy) way to get an element of a bitvector as an lvalue
static inline int bitvector_ref(struct bitvector bv, int idx){
  int byte = idx>>3;
  int bit = idx & 0x7;
  return (bv.bytes[byte]>>bit) & 0x1;
}
enum bitvector_op {
  BITVECTOR_SET,
  BITVECTOR_UNSET,
  BITVECTOR_FLIP,
};
static inline void bitvector_assign(struct bitvector bv, int idx, int op){
  int byte = idx>>3;
  int bit = idx & 0x7;
  uint8_t mask = 1<<bit;
  if(op == BITVECTOR_SET){
    bv.bytes[byte] |= mask;
  } else if(op == BITVECTOR_UNSET){
    bv.bytes[byte] &= ~mask;
  } else if(op == BITVECTOR_FLIP){
    bv.bytes[byte] ^= mask;
  }
}
static inline void bitvector_set(struct bitvector bv, int idx){
  bitvector_assign(bv,idx, BITVECTOR_SET);
}
static inline void bitvector_unset(struct bitvector bv, int idx){
  bitvector_assign(bv,idx, BITVECTOR_UNSET);
}
static inline void bitvector_flip(struct bitvector bv, int idx){
  bitvector_assign(bv,idx, BITVECTOR_FLIP);
}

  
  

//binary heap (code in heap.c)
typedef struct heap binary_heap;
struct heap {
  void **heap;
  int (*cmp)(void*,void*);
  int len;
  int mem;
};
binary_heap *make_new_heap(void *arr, int len, cmp_fun cmp);
binary_heap *convert_to_heap(void *arr, int len, int mem, cmp_fun cmp);
binary_heap *heap_sort(binary_heap *heap);
void* heap_pop(binary_heap *heap);
void heap_add(binary_heap *heap, void *new_element);
void destroy_heap(binary_heap *heap);
//these are static in heap.c
//void heapify(binary_heap *heap);
//void sift_up(binary_heap *heap, int index);
//void sift_down(binary_heap *heap, long root, long end);

/*
  Further data structures, with their own headers:
  svector.h: Simple dynamic array/vector
  rbtree.h: red/black tree
  hash.h: (optionally threadsafe) hashtable
*/

/*
  Filesystem abstractions (admittedly not very complex abstractions)
*/

#define get_access_mode(mode) (mode & O_ACCMODE)
/*
  Given a file access mode, of the type accepted by open return a string
  representing the access mode that can be passed to fopen.
*/
char * __attribute__((const)) filemode_bits_to_string(int mode);
off_t file_len_by_fd(int fd);
off_t file_len_by_filename(const char *filename);
off_t file_len_by_FILE(FILE *file);
int regular_filep_FILE(FILE* file);
int regular_filep_filename(const char *filename);
int regular_filep_fd(long fd);
char* read_file_to_string_fd(int fd, size_t *sz);
char* read_file_to_string_FILE(FILE *file, size_t *sz);
char* read_file_to_string_filename(const char *file, size_t *sz);
/*
  This emulates the file options of the 'test' command (or the -X perl fxns).
  It's really just a wrapper for stat, but it's eaiser to use.
*/
int filetest_filename(const char *filename, char test);
int filetest_fd(int fd, char test);
int filetest_FILE(FILE *file, char test);
/*
  Since this just define macros the _Generic is never acutally evaluated
  so this is valid in any version of C or C++, however they can only
  be used in C11.

  TODO: wrap this in an #if (defined _ISOC11_SOURCE)
  and define alternative versions for older C versions
*/
#define generic_file_macro(base_name, x, ...)       \
  _Generic((x),                                         \
           long  : CAT(base_name, fd),                      \
           int   : CAT(base_name, fd),                           \
           FILE* : CAT(base_name, FILE),                    \
           char* : CAT(base_name, filename),                 \
           const char* : CAT(base_name, filename))(x,##__VA_ARGS__)
#define file_len(x)                             \
  generic_file_macro(file_len_by_, x)
#define regular_filep(x)                                \
  generic_file_macro(regular_filep_, x)
#define read_file_to_string(x, szptr)                                   \
  generic_file_macro(read_file_to_string_, x, szptr)
#define filetest(x, test)                       \
  generic_file_macro(filetest_, x, test)

/*
   TODO: streams (adapt from scilisp code)
*/

/*
  Wrappers for mmap
*/
//mmap the file given by fd, return a pointer to the maping and
//if sz is not NULL store the size of the mapping in it.
//You almost alawys want the size (you need it to unmap the file)
//if shared is nonzero the mapping is shared, otherwise it is private
void *mmap_file(int fd, int shared, int prot, size_t *sz);
void *mmap_filename(const char *file, int shared, int prot, size_t *sz);
/*
  mmap an anonymous region of memory size bytes long and return a pointer to it
*/
void* mmap_anon(size_t size);
/*
  Byte array versions of string functions
*/
/* Akin to strspn/strcspn for byte arrays */
uint32_t memspn(const uint8_t *buf, uint32_t len,
                const uint8_t *accept, uint32_t len2);
uint32_t memcspn(const uint8_t *buf, uint32_t len,
                 const uint8_t *reject, uint32_t len2);
/*
  These do the same thing as strspn and friends, but expect a prepopulated
  table, rather than creating a table each time.
*/

uint32_t strspn_table(const uint8_t *str, const uint8_t accept[256]);
uint32_t strcspn_table(const uint8_t *str, const uint8_t reject[256]);
uint32_t memspn_table(const uint8_t *buf, uint32_t len,
                      const uint8_t accept[256]);
uint32_t memcspn_table(const uint8_t *buf, uint32_t len,
                       const uint8_t reject[256]);

//same as strdup/strdupa
void *memdup(const void *src, size_t sz);
#define memdupa(src, sz)                        \
  __extension__                                 \
  ({const uint8_t *dest = alloca(sz);           \
    memcpy(dest, src, sz);                      \
    dest;})

/*
  Functions for dealing with time, functions which return a time use
  clock_gettime if available, and gettimeofday otherwise.
  IDEA: Add a function which returns monotonic time 
        (eg. clock_gettime using CLOCK_MONOTONIC)g

  There are 3 formats of time used here:
    double precision floating point numbers, in seconds
    nanoseconds since the epoch, as a 64 bit integer
    struct timespec, nanoseconds since the epoch with nanoseconds
      and seconds stored in seperate integers
*/

/*
  Return the current time as a floating point number of seconds
*/
double float_time();
/*
  Return the current time as nanoseconds
*/
int64_t nano_time();
/*
  return a timespec with the current time, this is mostly for convience
*/
struct timespec get_current_time();
/*
  Same as the above functions, but for cpu time rather than realtime
*/
double float_cputime();
int64_t nano_cputime();
struct timespec get_cpu_time();

/*
  Convert between time representations
*/
double timespec_to_float(struct timespec t);
struct timespec float_to_timespec(double t);

double nsec_to_float(time_t nsec);
int64_t float_to_nsec(double t);

struct timespec nsec_to_timespec(int64_t nsec);
int64_t timespec_to_nsec(struct timespec);

//one function to turn a stuct timeval into a supported type
struct timespec timeval_to_timespec(struct timeval tv);
/*
  sleep for the numbor of seconds indicated by sleep time.
  float_sleep will return the ammount of time remaning if interupted
  float_sleep_full will always sleep for at least sleep_time
    seconds even if interupted.
*/
double float_sleep(double sleep_time);
void floatsleep_full(double sleep_time);
/*
  Wrappers around strto[u]l, they take an extra argument which
  is a pointer to put the result in, and they return 0/1 to
  indicate an error. 0 is stored in result on error.
*/
int strtol_checked(const char *nptr, char **endptr,
                   int base, long *result);
int strtoul_checked(const char *nptr, char **endptr,
                    int base, unsigned long *result);
/*
  Parse an integer in the given base, mostly the same as strtol
  but leading 0s are ignored instead of indicating octal.
  if there is an error errno is set and it is stored in err, if it's not null.
  It takes a length argument to support parsing of non-null
  terminated strings.

  This function actually does the string to integer convesrion itself,
  i.e it doesn't call strtol.

  This probably needs to be tested.
*/
long parse_integer(char *str, char **endptr,
  int len, int base, int *err);
/*
  Print a 'bitdepth' bit integer 'x' in binary to buf.
*/
char* binary_int_to_string(uint64_t x, int bitdepth, char *buf);
/*
  Create an array of ints starting at 'start' incrementing by 'step'
  and stopping at 'stop'.
  If one argument is provided it is taken as stop, start is set to 0,
    and step is set to 1.
  If two arguments are provide step is set to 1.
*/
int *iota(int start, int stop, int step);
#define IOTA(...) VFUNC(IOTA,...)
#define IOTA1(start)                            \
  iota(start, 0, 0)
#define IOTA2(start, stop)                      \
  iota(start, stop, 0);
#define IOTA3(start, stop, step)                \
  iota(start, stop, step)
/*
  Sorting functions
*/
/*
  Functions for sorting arrays of generic data.
*/
void insertion_sort_generic(void **input, size_t len, cmp_fun cmp);
//Qsort uses insertion sort once arrays get down to a certain size
//by default this is 4.
void qsort_generic(void **arr, size_t len, cmp_fun cmp);
void mergesort_generic(void **arr, size_t len, cmp_fun cmp);
/*
  Radix sort only works on integers, so these functions take a function
  pointer which should get an integer key from each input value, the
  input is sorted based on these integers.
*/
void radix_sort_u8_keys(void **in, size_t sz, uint64_t(*get_key)(void*));
void radix_sort_u16_keys(void **in, size_t sz, uint64_t(*get_key)(void*));
/*
  Functions for sorting unsigned 64 bit integers
 */
/*
  Format of names radix_sort_'hist_size'_'type_to_sort'.
*/
void radix_sort_u8_u64(uint64_t *in, size_t sz);
void radix_sort_u16_u64(uint64_t *in, size_t sz);
//alias of one of the two previous functions
void radix_sort_u64(uint64_t *in, size_t sz);

void radix_sort_u8_f64(double *double_in, size_t sz);
void radix_sort_u16_f64(double *double_in, size_t sz);
//alias of one of the two previous functions
void radix_sort_doubles(double *in, size_t sz);

void mergesort_u64(uint64_t *input, size_t len);
void qsort_u64(uint64_t *input, size_t len);
void insertion_sort_u64(uint64_t *input, size_t len);
void heapsort_u64(uint64_t *input, size_t len);
/*
  Random numbers, etc.
  The random number generator provided here is a xorshift+ generator,
  from 'Vigna, Sebastiano (April 2014).
         "Further scramblings of Marsaglia's xorshift generators".'
  It is fast, passes most statistical tests for randomness, has
  a 128 bit period, and requires very little code.

  The mersenne twister is probably faster for large quantities of
  random numbers, and has a much longer period, but the code is
  also much much more complicated.
*/
//you can't return an array in C so this needs to be a struct
typedef struct util_rand_state util_rand_state;
struct util_rand_state {
  uint64_t state[2];
};
//returns 64bit integers unifornly distributed between 0-2^64-1
uint64_t util_rand(void);
uint64_t util_rand_r(util_rand_state *state);
//double precison numbers in the range [0,1), with 53 bit precision
double util_drand(void);
double util_drand_r(util_rand_state *state);
//Return a random signed integer in the range min-max. This is actually
//a bit more complicated than it initally seems, as using mod results
//in an uneven distribution
int64_t util_rand_range_r(int64_t min, int64_t max, util_rand_state *state);
int64_t util_rand_range(int64_t min, int64_t max);
//returns the current random state, this is a copy, so changing it
//won't modify the internal state
util_rand_state util_get_rand_state(void);
//sets the internal random state to state and returns the old value
util_rand_state util_set_rand_state(util_rand_state *state);
//automatically generates a radnom state using a known form of randomness,
//usually either /dev/urandom or the current time.
util_rand_state util_auto_rand_state(void);
//initializes the random state
void util_srand(uint64_t a, uint64_t b);
void util_srand_auto(void);//calls util_auto_rand_state to get the state

//macros used to set how util_auto_rand_state works
#ifndef USE_SEED_URANDOM
#define USE_SEED_URANDOM 0
#endif
#ifndef USE_SEED_TIME
#define USE_SEED_TIME 1
#endif

void shuffle_array(void **arr, size_t len);
/*
  Approximate comparision of floating point numbers
*/
long double_ulp_diff(double a, double b);
int double_ulp_compare(double a, double b, int max_ulps);
int float_ulp_diff(float a, float b);
int float_ulp_compare(float a, float b, int max_ulps);
//Convert a floating point number (represented by an unsigned integer into
//an integer representation such that if x > y is true as floating point
//numbers than x > y will also be true as integers (this lets you sort 
//floating point numbers as integers)
uint64_t sign_and_magnitude_to_biased_64(double x);
uint32_t sign_and_magnitude_to_biased_32(float x);
/*
  Basic hash function, never know when you need a hash function.
*/
#define fnv_prime_64 1099511628211UL
#define fnv_offset_basis_64 14695981039346656037UL
static uint64_t fnv_hash(const void *key, size_t keylen){
  const uint8_t *raw_data=(const uint8_t *)key;
  size_t i;
  uint64_t hash=fnv_offset_basis_64;
  for(i=0; i < keylen; i++){
    hash = (hash ^ raw_data[i])*fnv_prime_64;
  }
  return hash;
}
/*
  Functionally identical to asprintf, but uses alloca to allocate
  memory on the stack instead of using malloc.
 */
#define asprintf_alloca(fmt, ...)                                       \
  __extension__ ({size_t sz = snprintf(NULL, fmt, 0, ##__VA_ARGS__);    \
      char *str = alloca(sz);                                           \
      snprintf(str, sz, fmt, ##__VA_ARGS__);                            \
      str;})
/*
  Malloc wrappers, which call a given function if out of memory, which
  by default prints an error message and aborts.

  xmalloc wraps malloc, xrealloc wraps realloc, and zmalloc wraps calloc.

  If UNCHECKED_ALLOC is defined then these are just defined as aliases to
  malloc, realloc and calloc (with the exception that zmalloc only takes 1 arg)
*/

#ifdef __cplusplus
}
#endif
#endif
