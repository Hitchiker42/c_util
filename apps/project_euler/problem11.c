#include "proj_euler.h"
#include "constants.h"
long max_nums[4] = {0};
#define grid problem11_constant
long index_max(int i,int j,long cur_max){
  if(j + 4 <= 20){
    long a = grid[i*20+j], b = grid[i*20+(j+1)];
    long c = grid[i*20+(j+2)], d = grid[i*20+(j+3)];
    long prod = a*b*c*d;
    if(prod > cur_max){
      cur_max = prod;
      max_nums[0] = a; max_nums[1] = b;
      max_nums[2] = c; max_nums[3] = d;
    }
  }
  if(i + 4 <= 20){
    long a = grid[i*20+j], b = grid[(i+1)*20+j];
    long c = grid[(i+2)*20+j], d = grid[(i+3)*20+j];
    long prod = a*b*c*d;
    if(prod > cur_max){
      cur_max = prod;
      max_nums[0] = a; max_nums[1] = b;
      max_nums[2] = c; max_nums[3] = d;
    }
  }
  if(i+4 <= 20 && j+4 <= 20){
    long a = grid[i*20+j], b = grid[(i+1)*20+(j+1)];
    long c = grid[(i+2)*20+(j+2)], d = grid[(i+3)*20+(j+3)];
    long prod = a*b*c*d;
    if(prod > cur_max){
      cur_max = prod;
      max_nums[0] = a; max_nums[1] = b;
      max_nums[2] = c; max_nums[3] = d;
    }
  }
  if(i-3 >=0 && j+4 <= 20){
    long a = grid[i*20+j], b = grid[(i-1)*20+(j+1)];
    long c = grid[(i-2)*20+(j+2)], d = grid[(i-3)*20+(j+3)];
    long prod = a*b*c*d;
    if(prod > cur_max){
      cur_max = prod;
      max_nums[0] = a; max_nums[1] = b;
      max_nums[2] = c; max_nums[3] = d;
    }
  }
  return cur_max;
}
int problem11(){
  int i,j;
  //if we only go upto 17 we don't actually need the checks in the preious function.
  long cur_max = 0;
  for(i=0;i<20;i++){
    for(j=0;j<20;j++){
      cur_max = MAX(cur_max, index_max(i,j,cur_max));
    }
  }
  printf("Max of 4 adjacent numbers is %ld\n",cur_max);
  printf("The numbers are %ld,%ld,%ld,%ld\n",max_nums[0],max_nums[1],
         max_nums[2],max_nums[3]);
  return 0;
}
