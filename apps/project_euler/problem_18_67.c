/*
  These are both the same problem, but with different inputs.
  the problem is to find the path in a binary tree
  (formed by a triangle of numbers) which maximizes the sum of numbers on
  the path.
*/
long problem18_input[] =
  {75
   95, 64
   17, 47, 82
   18, 35, 87, 10
   20, 04, 82, 47, 65
   19, 01, 23, 75, 03, 34
   88, 02, 77, 73, 07, 63, 67
   99, 65, 04, 28, 06, 16, 70, 92
   41, 41, 26, 56, 83, 40, 80, 70, 33
   41, 48, 72, 33, 47, 32, 37, 16, 94, 29
   53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14
   70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57
   91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48
   63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31
   04, 62, 98, 27, 23, 09, 70, 98, 73, 93, 38, 53, 60, 04, 23};
//This is a longest path problem, which is NP hard in the general case
//but since this is a tree it's a DAG, and there's a polynomial time
//algorithm for finding the longest path in a DAG.
/*
  The jist of the algorithm is we set the value of each node
  to its value the the value of its largest parent. For the starting
  node(s) it's just the value of the node. To recover the path
  we find the max value in the endi

*/
int *maximum_path_sum(long *tree, int nrows){
  int i,j;
  long len = (nrows*(nrows+1))/2;
  long *max_path_values = malloc(len*sizeof(long));
  max_path_values[0] = tree[0];
  long last_row = 0;
  long row;
  for(i=1;i<nrows;i++){
    row = (i*(i+1))/2;
    //first value in row only has right parent
    max_path_values[row] = max_path_values[last_row] + tree[row];
    for(j=1;j<i;j++){
      long max_val = MAX(max_path_values[last_row + j-1],
                         max_path_values[last_row + j]);
      max_path_values[row + j] = max_val + tree[row];
    }
    //last value in row only has left parent
    max_path_values[row+i] = max_path_values[row-1];
  }
  //find the value in the last row with max value and walk
  //backwards from there to get the path.
  //We need to scan the entire last row.
  int rowmax = 0;
  for(i=1;i<nrows;i++){
    if(max_path_values[i] > max_path_values[argmax]){
      rowmax = i;
    }
  }
  //after that we just need to check the parents,
  //we keep track of the row so we can know if there is only one parent
  //for row i, column j the parents are
  //row i-1, column j-1 and row i-1 column j, if j is either 0 or
  //the last value in the row then there's only one parent
  int *path = malloc(nrows*sizeof(int));
  path[nrows-1] = argmax;
  row = nrows-1;
  col = rowmax;
  for(i=(nrows-2);i>0;i--){
    int row_offset = (i*(i-1))/2;//((i-1)*(i-1+1))/2
    if(col == 0){
      path[i] = row_offset;
    } else if(col == i+1){
      path[i] = row_offset+i;
      col = i;
    } else {
      long p1 = row_offset + col-1;
      long p2 = row_offset + col;
      if(p1 > p2){
        path[i] = p1;
        col = col-1;
      } else {
        path[i] = p2;
      }
    }
  }
  path[0] = 0;
  return path;
}
