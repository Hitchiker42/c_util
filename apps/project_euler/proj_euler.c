#include "proj_euler.h"
/*
  This really shouldn't be in this repositiory but I don't want to deal with
  linking issues.
*/
#define max_problem 589
typedef int(*problem_fn)();
problem_fn problems[max_problem+1] = {NULL};
void init_problems(){
  void *handle = dlopen(NULL, RTLD_NOW);
  char buf[16];
  int i;
  for(i=0;i<max_problem;i++){
    snprintf(buf,16,"problem%d",i);
    void *problem = dlsym(handle, buf);
    if(problem){
      problems[i] = problem;
    }
  }
  //Check for a problem we know we should have, and fail if
  //we can't get it
  if(problems[10] == NULL){
    dlsym(handle,"problem10");//retry to get the error string
    fputs("Failed to properly load functions with error:",stderr);
    fputs(dlerror(),stderr);
    fputs("\n",stderr);
    
    exit(1);
  }
}
long max_problem_number = max_problem;
int main(int argc, char* argv[]){
  init_problems();
  if(argc < 2){
    printf("Usage ./proj_euler problem_number\n");
    exit(1);
  }
  int problem_number = strtol(argv[1],NULL,10);
  if(problem_number > max_problem_number){
    printf("Problem number %d is greater than any existing problem (max = %ld)\n",
           problem_number, max_problem_number);
    exit(1);
  }
  if(problems[problem_number] == NULL){
    printf("No solution currently exists for problem %d\n",problem_number);
    exit(1);
  }
  return problems[problem_number]();
}
