#ifndef __PROJ_EULER_H__
#define __PROJ_EULER_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "C_util.h"
#include "math_util.h"
typedef union pythag_triple pythag_triple;
union pythag_triple {
  struct {
    long a;
    long b;
    long c;
  };
  long abc[3];
};
int base_10_digits(unsigned long n, uint8_t *digits);
long collatz(long n);
ulong primes_to_n(ulong n, ulong *buf, size_t bufsz);
ulong prime_count_upper_bound(ulong n);
void pythag_triple_children(pythag_triple root, pythag_triple *children);
pythag_triple* gen_pythag_triple_children(pythag_triple root);
#ifdef __cplusplus
}
#endif
#endif /* __PROJ_EULER_H__ */
