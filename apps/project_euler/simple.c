#include "proj_euler.h"
#include "constants.h"
/*
  Since these are generally run effectively as their own programs
  I don't really need to worry about freeing memory, but I do since
  it's worth being in the habit to do so.
*/
int problem7(){
  ulong *buf = malloc(10001*sizeof(long));
  if(primes_to_n(1000000,buf,10001) < 10001){
    printf("Error less than 10001 primes less than 1000000\n");
    free(buf);
    return 1;
  }
  printf("10001st prime is %ld\n",buf[10000]);
  free(buf);
  return 0;
}
int problem10(){
  int i;
  size_t bufsz = prime_count_upper_bound(2000000);
  ulong *buf = malloc(bufsz*sizeof(long));
  long prime_count = primes_to_n(2000000,buf,bufsz);
  ulong sum = 0;
  for(i=0;i<prime_count;i++){
    if(sum + buf[i] < sum){
      printf("Error overflow on %dth prime\n",i);
      return 1;
    }
    sum += buf[i];
  }
  printf("Sum of the %ld primes < 2000000 = %lu\n",prime_count,sum);
  return 0;
}
int problem14(){
  long i;
  long cur_max = 0;
  long cur_max_val = 1;
  for(i=2;i<=1000000;i++){
    long cnt = collatz(i);
    if(cnt > cur_max){
      cur_max = cnt;
      cur_max_val = i;
    }
  }
  printf("Value with max chain = %ld (length is %ld)\n", cur_max_val,cur_max);
  return 0;
}
/*
  Find the Pythagorean triple a,b,c such that a+b+c = 1000,
  return its product.
*/
int problem9(){
  pythag_triple *mem = malloc(sizeof(pythag_triple)*1000);
  mem[0] = (pythag_triple){.abc = {3,4,5}};
  //Bredth first search over the ternary tree of Pythagorean triples
  struct queue *q = make_queue();
  queue_push(q,mem);
  int offset = 1;
  //the queue will never be empty, and we just assume we'll find
  //a solution
  while(offset<1000){
    pythag_triple *root = queue_pop(q);
    long sum = (root->a + root->b + root->c);
    if((1000 % sum) == 0){
      long k = 1000/sum;
      long a = root->a*k, b = root->b*k, c = root->c*k;
      printf("%ld^2(%ld) + %ld^2(%ld) = %ld^2(%ld)\n"
             "%ld + %ld + %ld = 1000\n"
             "%ld * %ld * %ld = %ld\n",
             a,a*a,b,b*b,c,c*c,a,b,c,a,b,c,a*b*c);


      free(mem);
      return 0;
    }
    pythag_triple *children = mem + offset;
    offset += 3;
    pythag_triple_children(*root,children);
    queue_push(q,children+0);
    queue_push(q,children+1);
    queue_push(q,children+2);
  }
  printf("Error no correct triple found\n");
  free(mem);
  return -1;
}
int problem25(){
  mpz_t fn,fn1,fn2;
  long n = 10;
  mpz_inits(fn,fn1,fn2,NULL);
  mpz_fib2_ui(fn1,fn,n);
  mpz_add(fn2,fn,fn1);
  n++;
  while(mpz_sizeinbase(fn2,10) < 1000){
    //    gmp_printf("the %ldth fibonacci number is %Z\n",n,fn2);
    mpz_swap(fn,fn1);
    mpz_swap(fn1,fn2);
    mpz_add(fn2,fn,fn1);
    n++;
  }
  gmp_printf("The %ldth fibonacci number is the first with 1000 digits,"
             "it is:\n%Zd\n"
             "The prior 2 fibonacci numbers are:%Zd\n%Zd\n",n,fn2,fn1,fn);
  mpz_clears(fn,fn1,fn2,NULL);
  return 0;
}
int problem13(){
  mpz_t sum, acc;
  char *str = NULL;
  int i;
  mpz_inits(sum,acc,NULL);
  mpz_set_str(sum, problem13_constant[0], 10);
  for(i=1;i<100;i++){
    mpz_set_str(acc, problem13_constant[i], 10);
    mpz_add(sum,sum,acc);
  }
  str = mpz_get_str(str,10,sum);
  printf("First 10 digits of the sum are:\n");
  for(i=0;i<10;i++){
    putchar(str[i]);
  }
  putchar('\n');
  free(str);
  mpz_clears(sum,acc,NULL);
  return 0;
}
static ulong square_digits(uint8_t *digits, int ndigits){
  int i;
  ulong sum = 0;
  for(i=0;i<ndigits;i++){
    sum += digits[i]*digits[i];
  }
  return sum;
}
//every square digit chain ends at 1 or 89, we need to count the
//number that end at 89, this returns 1 if the given number
//ends at 89, and 0 if it ends at 1.
static int square_digit_chain(ulong num){
  static uint8_t digits[20];
  static int ndigits;
  while(num != 89 && num != 1){
    ndigits = base_10_digits(num, digits);
    num = square_digits(digits, ndigits);
  }
  return num == 89;
}
int problem89(){
  ulong num;
  ulong cnt = 0;
  for(num = 1;num < 10000000;num++){
    if(!(num % 100000)){
      printf("On number %ld, current count %ld\n",num,cnt);
    }
    cnt += square_digit_chain(num);
  }
  printf("The number of square digits chains below"
         "10,000,000 that end at 89 is: %ld\n",cnt);
  return 0;
}
