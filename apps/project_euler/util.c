#include "proj_euler.h"
/*
  Some of these should probably be moved to the math library
*/
long collatz(long n){
  long cnt = 0;
  while(n != 1){
    if(n % 2){
      n = 3*n + 1;
    } else {
      n = n/2;
    }
    cnt++;
  }
  return cnt;
}
//Digits needs to be either NULL or a buffer of length 20 or more,
//if it is NULL a buffer of length 20 will be allocated
int base_10_digits(unsigned long n, uint8_t *digits){
  uint8_t buf[20];//20 decimal digits is the max that can be stored in 64bits
  uint8_t *bufptr = buf+20;
  int cnt = 0;
  while(n){
    *--bufptr = (n % 10);
    n /= 10;
    cnt++;
  }
  int i;
  if(digits == NULL){
    digits = malloc(20);
  }
  for(i=0;i<cnt;i++){
    digits[i] = *bufptr++;
  }
  return cnt;
}
//Essentially Unoptimized sieve of eratosthenes
ulong primes_to_n(ulong n, ulong *buf, size_t bufsz){
  struct bitvector odds = make_bitvector((n/2),1);
  ulong max = floor(sqrt(n));
  size_t cnt = 1;
  buf[0] = 2;
  ulong i,j;
  for(i=3;i<max;i+=2){
    if(bitvector_ref(odds,(i/2))){
      buf[cnt++] = i;
      if(cnt == bufsz){return cnt;}
      for(j=i;i*j<n;j++){
        if(i*j % 2){
          bitvector_unset(odds,(i*j)/2);
        }
      }
    }
  }
  for(;i<n;i+=2){
    if(bitvector_ref(odds,(i/2))){
      buf[cnt++] = i;
      if(cnt == bufsz){return cnt;}
    }
  }
  return cnt;
}
/* ulong primes_from_m_to_n(ulong m, ulong n, ulong *buf, size_t buflen, size_t bufsz){ */
/*   struct bitvector odds = make_bitvector(((n-m)/2),1); */
/*   ulong max = floor(sqrt(n)); */
/*   ulong i,j; */
/*   for(i=0;i<buflen;i++){ */
/*     ulong x = buf[i]; */
/*     ulong y = x; */
/*     while(y*x < m){y++;} */
/*     while(y*x < n){ */
/*       if(y*x % 2){ */
/*         bitvector_unset(((y*x)-m)/2 */
/*       while(j*x < m){x++}; */


/*   size_t cnt = 1; */
/*   for(i=3;i<max;i+=2){ */
/*     if(bitvector_ref(odds,(i/2))){ */
/*       buf[cnt++] = i; */
/*       if(cnt == bufsz){return cnt;} */
/*       for(j=i;i*j<n;j++){ */
/*         if(i*j % 2){ */
/*           bitvector_unset(odds,(i*j)/2); */
/*         } */
/*       } */
/*     } */
/*   } */
/*   for(;i<n;i+=2){ */
/*     if(bitvector_ref(odds,(i/2))){ */
/*       buf[cnt++] = i; */
/*       if(cnt == bufsz){return cnt;} */
/*     } */
/*   } */
/*   return cnt; */
/* } */

ulong prime_count_upper_bound(ulong x){
  if(x > 60184){
    return (ulong)(ceil(x/(log(x)-1.1)));
  } else {
    return ((x/log(x)) * (1 +  1.2762/log(x)));
  }
}

/* svector factor(ulong x){ */
/*   static size_t max_n = 1000000; */
/*   static size_t bufsz = 100000; */
/*   static ulong *primes = malloc(bufsz*sizeof(ulong)); */
/*   static size_t nprimes = primes_to_n(max_n,primes,bufsz); */
/*   if(x > max_n){ */
/*     max_n = MAX(x,max_n*2); */
/*     bufsz = prime_count_upper_bound(max_n); */
/*
  The primitive pythagorean triples form a ternary tree.
  Given a pythagorean triple a,b,c its 3 children are formed by multiplying
  the column matrix [a,b,c] by the following 3 matrices:
      1 -2 2      1 2 2      -1 2 2
  A = 2 -1 2, B = 2 1 2, C = -2 1 2
      2 -2 3      2 2 3      -2 2 3
*/
void mat3_vec_mul_simple(const long*  A,
                         long *restrict x,
                         long *restrict b){
  for(int i=0;i<3;i++){
    b[i] = A[i*3 + 0]*x[0] + A[i*3 + 1]*x[1] + A[i*3 + 2]*x[2];
  }
}
void pythag_triple_children(pythag_triple root, pythag_triple *children){
  static long A[9] = {1,-2,2,2,-1,2,2,-2,3};
  static long B[9] = {1,2,2,2,1,2,2,2,3};
  static long C[9] = {-1,2,2,-2,1,2,-2,2,3};
  mat3_vec_mul_simple(A,root.abc, children[0].abc);
  mat3_vec_mul_simple(B,root.abc, children[1].abc);
  mat3_vec_mul_simple(C,root.abc, children[2].abc);
}
pythag_triple* gen_pythag_triple_children(pythag_triple root){
  pythag_triple *children = malloc(3*sizeof(pythag_triple));
  pythag_triple_children(root,children);
  return children;
}
