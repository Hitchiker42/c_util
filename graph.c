#include "graph.h"
typedef struct graph graph;
typedef struct graph_node node;
typedef struct graph_edge edge;
typedef void(*visit_func)(node *,void*);

#define edge_source(g,e) g->nodes[e->from]
#define edge_dest(g,e) g->nodes[e->to]

#define graph_node_count(g) g->nodes_len
#define graph_edge_count(g) g->edges_len


graph* make_graph(int edges_size, int nodes_size){
  graph* g = zmalloc(sizeof(graph));
  g->edges_vec = make_svector(edges_size);
  g->nodes_vec = make_svector(nodes_size);
  return g;
}
void destroy_graph(graph *g){
  int i;
  svector_mapc(&(g->edges_vec), destroy_edge);
  svector_mapc(&(g->nodes_vec), destroy_node);
  free(g);
}
edge* make_edge(int from, int to, void *data){
  edge* e = malloc(sizeof(edge));
  e->from = from;
  e->to = to;
  e->data = data;
  return e;
}
//currently just frees the edge, but if I add other data to the edge I
//won't need to change other bits of code.
void destroy_edge(edge *e){
  free(e);
}
edge* make_weighted_edge(int from, int to, double weight){
  edge *e = malloc(sizeof(edge));
  e->from = from;
  e->to = to;
  e->weight = weight;
  return e;
}
node* make_node(void *data){
  node* n  = zmalloc(sizeof(node));
  n->edges_vec = make_svector(8);
  n->data = data;
  return n;
}
void destroy_node(node *n){
  destroy_svector(&(n->edges_vec));
  free(n);
}
int graph_add_edge(graph *g, int from, int to, void *data){
  edge *e = make_edge(from,to,data);
  e->id = graph_edge_count(g);
  SVECTOR_PUSH(e, g->edges_vec);
  SVECTOR_PUSH(e, g->nodes[from]->edges_vec);
  return e->id;
}
/*
  Add add edges from-to and to-from, they get the same data.
*/
int graph_add_dual_edge(graph *g, int from, int to, void *data){
  edge *e1 = make_edge(from,to,data);
  edge *e2 = make_edge(to,from,data);
  e1->id = graph_edge_count(g);
  SVECTOR_PUSH(e1, g->edges_vec);
  SVECTOR_PUSH(e1, g->nodes[from]->edges_vec);
  e2->id = graph_edge_count(g);
  SVECTOR_PUSH(e2, g->edges_vec);
  SVECTOR_PUSH(e2, g->nodes[to]->edges_vec);
  return e2->id;
}
int graph_add_node(graph *g, void *data){
  node* n = make_node(data);
  n->id = graph_node_count(g);
  SVECTOR_PUSH(n, g->nodes_vec);
  return n->id;
}

//Should only be used when building a graph
static void graph_set_node(graph *g, int id, void *data){
  node* n = make_node(data);
  n->id = id;
  g->nodes[id] = n;
}
static void graph_set_edge(graph *g, int id, int from, int to, void *data){
  edge* e = make_edge(from, to, data);
  e->id = id;
  g->edges[id] = e;
  //still need to use push here
  SVECTOR_PUSH(e, g->nodes[from]->edges_vec);
}
graph *build_graph(int num_edges, int num_nodes,
                   int *from, int *to,
                   void **edge_data, void **node_data){
  int i;
  graph *g = make_graph(num_edges, num_nodes);
  //This temporally puts the graph in an inconsistant state
  g->edges_len = num_edges;
  g->nodes_len = num_nodes;
  //gcc could probably lift the conditional out of the loop, but eh
  if(node_data){
    for(i=0;i<num_nodes;i++){
      graph_set_node(g, i, node_data[i]);
    }
  } else {
    for(i=0;i<num_nodes;i++){
      graph_set_node(g, i, NULL);
    }
  }
  if(edge_data){
    for(i=0;i<num_edges;i++){
      graph_set_edge(g, i, from[i], to[i], edge_data[i]);
    }
  } else {
    for(i=0;i<num_edges;i++){
      graph_set_edge(g, i, from[i], to[i], NULL);
    }
  }
  return g;
} 
static void zero_temp_data(graph *g){
  int i;
  for(i=0; i < graph_node_count(g); i++){
    memset(g->nodes[i]->temp_data, 0, sizeof(g->nodes[i]->temp_data));
  }
}
static void set_temp_data(graph *g, void *val){
  int i;
  for(i=0; i < graph_node_count(g); i++){
    memcpy(g->nodes[i]->temp_data, val, sizeof(g->nodes[i]->temp_data));
  }
}

// bfs
static void set_bfs_data(node *n, int dist, int color, int parent){
  n->bfs_data.dist = dist;
  n->bfs_data.color = color;
  n->bfs_data.parent = parent;
}
#define bfs_dist(n) n->bfs_data.dist
#define bfs_color(n) n->bfs_data.color
#define bfs_parent(n) n->bfs_data.parent
//It wouldn't be too hard to make a version of this without the
//visit callback, by marking this always inline and provding a
//dummy function
void graph_bfs(graph *g, unsigned int start,
         //maybe visit should also take the graph as an argument
         void(*visit)(node*,void*), void *data){
  struct queue * q = make_queue();
  int i;
  set_bfs_data(g->nodes[start], 0, 1, -1);
  queue_push(q, g->nodes[start]);
  while(!queue_is_empty(q)){
    node *n = queue_pop(q);
    //Add nodes reachable from n to the queue
    for(i=0; i < n->edge_count; i++){
      node *m = edge_dest(g, n->edges[i]);
      if(bfs_color(m) == 0){
        set_bfs_data(m, 1, bfs_dist(n)+1, n->id);
        queue_push(q, m);
      }
    }
    //visit n, and mark it as visited
    if(visit){
      visit(n, data);
    }
    bfs_color(n) = 1;
  }
  return;
}

#define dfs_disc(n) n->dfs_data.disc
#define dfs_fin(n) n->dfs_data.fin
#define dfs_color(n) n->dfs_data.color
#define dfs_parent(n) n->dfs_data.parent
static void dfs_init(graph *g){
  int i;
  for(i=0; i < graph_node_count(g); i++){
    //we don't need to init timestamps since, for the purposes
    //of dfs they're write only
    dfs_color(g->nodes[i]) = 0;
    dfs_parent(g->nodes[i]) = -1;
  }
}
static void dfs_recur(graph *g, node *n, int *time,
                      void(*visit)(node*, void*), void *data){
  int i;
  dfs_disc(n) = *(++time);
  dfs_color(n) = 1;
  for(i=0; i < n->edge_count; i++){
    node *m = edge_dest(g,n->edges[i]);
    if(dfs_color(m) == 0){
      dfs_parent(m) = n->id;
      dfs_recur(g, m, time, visit, data);
    }
  }
  visit(n, data);
  dfs_fin(n) = *(++time);
}
void graph_dfs(graph *g, void(*visit)(node*,void*), void *data){
  int i, time = 0, node_count = graph_node_count(g);
  node *n;
  for(i=0; i < g->nodes_len; i++){
    n = g->nodes[i];
    if(dfs_color(n) == 0){
      dfs_recur(g, n, &time, visit, data);
    }
    /*
      Break if we've already visited all of the vertices.
      This is a fairly important optimization, it doesn't change the complexity
      but tends to give a mesurable speed up in practice, since most graphs are
      either fully connected, or divided into a number of unconnected
      components which is significantly less then the number of vertices.
      Meaning we'll have visited all the vertices long before this loop finishes.
    */
    if(time >= (node_count*2)){
      break;
    }
  }
  return;
}
/*
//Code for a topological sort using an array of ids to store the result
//The array will be in reverse order after dfs is finished and so
//will need to be reversed.
struct top_sort_data {
  unsigned int *sorted;
  int idx;
};
static void top_visit(node *n, struct top_sort_data *data){
  data->sorted[data->idx++] = n->id;
}
*/
static void top_visit(node *n, cons_t **ls){
  CONS_PUSH(n, ls);
}
cons_t* topological_sort(graph *g){
  cons_t *ls = NULL;
  graph_dfs(g, (visit_func)top_visit, &ls);
  return ls;
}
/*
  Breaks a DAG into it's strongly connected components,
  strongly connected components are nodes where each node is 
  reachable from every other node in the component.

  example use:
    By make to generate dependencies to allow for independent parts
    to be built in parallel.

  This is Tarjans algorithm for strongly connected components, this isn't
  in intro to algorithms, but its faster than the algorithm they do give.
*/
#define sc_link(n) (n->strong_connect_data.link)
#define sc_index(n) (n->strong_connect_data.index)
#define sc_on_stack(n) (n->strong_connect_data.on_stack)
static void strong_connect(graph *g, node *n, int *index,
                           svector *stack, svector *components){
  /*
    Each node has two values: index and link, index is set once and
    is unique for every node. link is initially set to index, and is
    set to the value of the lowest index reachable frome the given node.
    A stack is maintained of all nodes currently reachable, when a node
    is finished being visited and still has link equal to its index then it
    is the root of a strongly connected component. The stack is then emptied
    and turned into a list which is added to the components vector.
  */
  
  sc_index(n) = ++(*index);
  sc_link(n) = sc_index(n);
  svector_push(n, stack);
  sc_on_stack(n) = 1;

  int i;
  for(i=0; i < n->edge_count; i++){
    node *m = edge_dest(g, n->edges[i]);
    if(sc_index(m) == 0){
      strong_connect(g, m, index, stack, components);
      sc_link(n) = MIN(sc_link(n), sc_link(m));
    } else if(sc_on_stack(m)){
      sc_link(n) = MIN(sc_link(n), sc_link(m));
    }
  }
  if(sc_link(n) == sc_index(n)){
    cons_t *ls = NULL;
    node *m;
    do {
      m = svector_pop(stack);
      sc_on_stack(m) = 0;
      CONS_PUSH(m, &ls);
    } while (m != n);
    svector_push(ls, components);
  }
}
  
svector strongly_connected_components(graph *g){
  int i, index = 1;
  zero_temp_data(g);
  svector stack = make_svector(32);
  svector components = make_svector(16);
  for(i=0; i < graph_node_count(g); i++){
    if(sc_index(g->nodes[i]) == 0){
      strong_connect(g, g->nodes[i], &index, &stack, &components);
    }
    //Might be able to optimize by breaking once we've visited all
    //the vertices, but we need to make sure to empty the stack.
  }
  return components;
}
#define sp_parent(n) (n->shortest_path_data.parent)
#define sp_index(n) (n->shortest_path_data.heap_index)
#define sp_cost(n) (n->shortest_path_data.cost)
static int node_cost_cmp(node *n1, node *n2){
  return sp_cost(n1) < sp_cost(n2);
}
static void init_sp_data(node *n){
  n->cost = INFINTITY;
  n->parent = 0;
  n->heap_index = -1;//indicate that it's not on the heap.
}  
//Computes the shortest path from src to dst. set dst to a value larger
//than the number of nodes to find all shortests paths.
//mem is a block of memory large enough to hold pointers to all
//the vertices of the graph and is used as the heap in the algorithm.
//it can be reusued to hold the path itself with a bit of work.
static void shortest_path_internal(graph *g, uint src, uint dst, void *mem){
  int i;
  int nnodes = graph_node_count(g);  
  binary_heap *heap = convert_to_heap(mem, 0, nnodes*sizeof(node*),
                                      node_cost_cmp);
  for(i=0;i<nnodes;i++){
    init_sp_data(g->nodes[i]);
  }
  node *start = g->nodes[src];
  sp_cost(start) = 0;
  sp_index(start) = 0;
  //we know the heap has enough memory so we can avoid checking
  //if we have enough memory when adding to the heap
#define push(heap,elt)                          \
  ({heap->heap[heap->len++] = elt;              \
    heap_sift_up(heap,heap->len-1);})
#define pop heap_pop

  push(heap,start);
  node *n;
  while(heap->len > 0){
    n = heap_pop(heap);
    n->heap_index = -1;
    if(n->id == dst){break;}//found our destination
    for(i=0;i<n->edge_count;i++){
      edge *e = n->edges[i];
      node *m = g->nodes[e->dst];
      if(sp_cost(m) > sp_cost(n) + e->weight){
        sp_cost(m) = sp_cost(n) + e->weight;
        sp_parent(m) = n;
        if(m->heap_index == -1){
          m->heap_index = push(heap,m);
        } else {
          m->heap_index = heap_sift_up(heap,sp_index(m));
        }
      }
    }
  }
  free(heap);
}
//returns a pointer so it can return NULL when no path is found.
svector* shortest_path(graph *g, int src, int dst, double *cost){
  //memory for the heap
  void *mem = malloc(graph_node_count(g)*sizeof(node*));
  shortest_path_internal(g,src,dst,mem);
  node *n = g->nodes[dst];
  if(sp_cost(n) == INFINITY){
    free(mem);
    return NULL;
  }
  if(cost != NULL){
    *cost = sp_cost(n);
  }
  //build the path in the memory used for the heap to figure
  //out its size, then copy it into fresh memory, on the assumption
  //that the path will be much smaller than the total size of the graph.
  int idx = 0;
  while(n->id != start){
    mem[idx++] = n;
    n = n->parent;
  }
  mem[idx++] = g->nodes[src];
  int len = idx;
  svector *ret = malloc(sizeof(svector));
  ret->len = len;
  ret->size = len;
  ret->data = malloc(len * sizeof(node*));
  memcpy(ret->data, mem, len*sizeof(node*));
  free(mem);
  return ret;
}
/*
  Print a graph to a file using the DOT language.

  At least for now heres the relevent syntax.
  graph: ('graph' | 'digraph') [ID] { stmts }
  stmts: stmt [;] stmts
  //stmt: node_stmt | edge_stmt | attr_stmt | ID '=' ID | subgraph
  stmt: node_stmt | edge_stmt 
  node_stmt: node_id [attrs]
  edge_stmt: node_id edgeRHS
  //-> is for directed graphs, -- for nondirected
  edgeRHS: (-> | --) node_id [edgeRHS]
  attrs: '[' a_list ']' [attrs]
  a_list: ID '=' ID [';'|',']] [a_list]

  For now I'm ignoring attributes
  
*/
int print_graph(graph *g, char *filename,
                char*(*get_node_name)(node *,void*), void*data){
  FILE *f = fopen(filename, "w");
  if(f == NULL){
    perror("fopen");
    return -1;
  }
  fputs("digraph {\n", f);
  zero_temp_data(g);
  int i;
  edge *e;
  node *n,*m;
  //Print all edges, keeping track of what nodes were
  //printed (we need to do this to print unconnected nodes)
  for(i=0;i<graph_edge_count(g);i++){
    e = g->edges[i];
    n = edge_source(g, e);
    m = edge_dest(g, e);
    n->state = 1;
    m->state = 1;
    if(get_node_name){
      fputs(get_node_name(n, data), f);
      fputs(" -> ", f);
      fputs(get_node_name(m, data), f);
      fputs("\n", f);
    } else {
      fprintf(f, "%u -> %u\n", n->id, m->id);
    }
  }
  for(i=0;i<graph_node_count(g);i++){
    n = g->nodes[i];
    if(n->state == 0){
      if(get_node_name){
        fputs(get_node_name(n, data), f);
        fputs("\n",f);
      } else {
        fprintf(f,"%u\n", n->id);
      }
    }
  }
  fputs("}", f);
  fclose(f);
  return 0;
}
