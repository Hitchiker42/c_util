#ifndef __GRAPH_H__
#define __GRAPH_H__
#ifdef __cplusplus
extern "C" {
#endif
//Currently this requires libcutil. Specifically it requires svectors
//and a queue.
#include "C_util.h"

/*
  There are a lot of ways of representing graphs, Since this is intendeded
  to be a general library/Example of graph algorithms the representation used
  is very general and has a lot of redundant data.
*/
struct graph {
  union {
    svector nodes_vec;
    struct {
      struct graph_node **nodes;
      int nodes_len;
      int nodes_size;
    };
  };
  union {
    svector edges_vec;
    struct {
      struct graph_edge **edges;
      int edges_len;
      int edges_size;
    };
  };
};
struct graph_node {
  void *data;//arbitrary data
  //vector of edges
  union {
    svector edges_vec;
    struct {
      struct graph_edge **edges;
      int edge_count;
      int edges_size;
    };
  };
      
  unsigned int id;//unique id / index
  //data needed for various algorithms
  //Only the data for the most recent algorithm is valid.
  union {
    int state;
    int temp_data[4];//should be the same size as the largest union member.
    struct {
      int dist;
      int parent;
      int color;
    } bfs_data;
    struct {
      int disc;
      int fin;
      int parent;
      int color;
    } dfs_data;
    struct {
      int index;
      int link;
      int on_stack;
    } strong_connect_data;
    struct {
      int heap_index;
      int parent;
      double cost;
    } shortest_path_data;
  };
};
struct graph_edge {
  //slight optimization for weighted edges vs edeges with arbitrary data
  union {
    void *data;
    double weight;
  };
  unsigned int from;
  unsigned int to;
  unsigned int id;
};
/*
  Building graphs
*/
struct graph* make_graph(int edges_size, int nodes_size);
void destroy_graph(struct graph *g);
struct graph_edge* make_edge(int from, int to, void *data);
struct graph_edge* make_weighted_edge(int from, int to, double weight);
void destroy_edge(struct graph_edge *e);
struct graph_node* make_node(void *data);
void destroy_node(struct graph_node *n);
int graph_add_edge(struct graph *g, int from, int to, void *data);
int graph_add_dual_edge(struct graph *g, int from, int to, void *data);
int graph_add_node(struct graph *g, void *data);
struct graph *build_graph(int num_edges, int num_nodes,
                          int *from, int *to,
                          void **edge_data, void **node_data);

void graph_bfs(struct graph *g, unsigned int start,
         void(*visit)(struct graph_node*,void*), void *data);
void graph_dfs(struct graph *g, void(*visit)(struct graph_node*,void*), void *data);
cons_t* topological_sort(struct graph *g);
svector strongly_connected_components(struct graph *g);
int print_graph(struct graph *g, char *filename,
                char*(*get_node_name)(struct graph_node *,void*), void *data);
#ifdef __cplusplus
}
#endif
#endif /* __GRAPH_H__ */
