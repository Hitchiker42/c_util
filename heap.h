#ifndef _MY_HEAP_H
#define _MY_HEAP_H
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
typedef struct heap binary_heap;
struct heap {
  void **heap;
  int (*cmp)(void*,void*);
  int len;
  int mem;
};
binary_heap* make_new_heap(void *arr, int size, cmp_fun cmp);
binary_heap *make_empty_heap(int sz, cmp_fun cmp);
binary_heap *convert_to_heap(void *arr, int len, int mem, cmp_fun cmp);
binary_heap* heap_sort(binary_heap *heap);
int heap_sift_up(binary_heap *heap, int index);
void heapify(binary_heap *heap);
void* heap_pop(binary_heap *heap);
void heap_add(binary_heap *heap,void *new_element);
int heap_empty(binary_heap *heap);
#endif
