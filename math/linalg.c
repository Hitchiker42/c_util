#define UNCHECKED_ALLOC
#include "math_util.h"
#include "matrix.h"

/*
  All the matrix algorithms here are deliberately written using only level 1
  blas calls, any matrix computations are done explicitly.
  The main point of this is to write and understand the algorithms
  not to write the fastest implementation.
*/
/* dmatrix column_slice(dmatrix A, int col){ */
/*   dmatrix ret = {A.mat +  */
//Compute the transpose of the square matrix A inplace, with
//awful cache performance
void square_transpose(dmatrix A){
  int i,j,k;
  int n = A.rows;
  for(i=0;i<n-1;i++){
    for(j=i+1;j<n;j++){
      matrix_swap(A,i,j,j,i);
    }
  }
}
//||A||_inf = max of sum of absolute values of rows
double inf_norm(dmatrix A){
  double norm;
  int i;
  norm = cblas_dasum(A.cols, A.mat, 1);
  for(i=1;i<A.rows;i++){
    double tmp = cblas_dasum(A.cols, A.mat + i*A.stride, 1);
    norm = MAX(norm,tmp);
  }
  return norm;
}
//||A||_1 = max of sum of absolute values of columns
double one_norm(dmatrix A){
  double norm;
  int i;
  norm = cblas_dasum(A.rows, A.mat, A.stride);
  for(i=1;i<A.cols;i++){
    double tmp = cblas_dasum(A.rows, A.mat + i, A.stride);
    norm = MAX(norm,tmp);
  }
  return norm;
}
double frob_norm(dmatrix A){
  double sum = 0.0;
  int i;
  for(i=0;i<A.rows;i++){
    sum += cblas_dnrm2(A.cols, A.mat + i*A.stride, 1);
  }
  return sqrt(sum);
}

/*
//Better square transpose, but I need to figure out how to swap the
//off-diagonal submatrices first.
void square_transpose(dmatrix A){
  int n = A.rows;
  if(n < 5){
    naive_square_transpose(A);
  } else {
    dmatrix A1 = {A.mat,n/2,n/2,n};//top left
    dmatrix A2 = {A.mat + n/2,n/2,(n+1)/2};//top right
    dmatrix A3 = {A.mat + (n*((n+1)/2)),(n+1)/2,n/2};//bottom left
    dmatrix A4 = {A.mat + (n/2 + (n*((n+1)/2))),(n+1)/2,(n+1)/2};//bottom right
    square_transpose(A1);
    square_transpose(A2);
    square_transpose(A3);
    square_transpose(A4);
    //Swap A2 and A3 somehow
    return;
  }
}
*/
/*
  Solve Ax = b using an LUP decomposition of A.
  First solve Ly = Pb using forward substution
  Then using the solution y solve Ux = y to get x.
*/
double* lup_solve(dmatrix LU, int *p, double *b){
  int n = LU.rows;
  int i,j;
  double y[n];
  double *x = xmalloc(n * sizeof(double));
  //Solve Ly = Pb
  for(i=0;i<n;i++){
    double sum = 0.0;
    for(j=0;j<i-1;j++){
      sum += matrix_ref(LU,i,j)*y[j];
    }
    y[i] = b[p[i]] - sum;
  }
  //Solve Ux = y
  for(i=n-1;i>=0;i--){
    double sum = 0.0;
    for(j=i;j<n;j++){
      sum += matrix_ref(LU,i,j)*x[j];
    }
    x[i] = (y[i] - sum)/matrix_ref(LU,i,i);
  }
  return x;
}
double lup_det(dmatrix LU, int *p){
  //If A = PLU then det(A) = det(P)*det(L)*det(U)
  //since L/U are triangular det(L/U) = prod(L/U[i,i],i=0...n)
  //The determinate of P is 1 if the number of permutations is even
  //and -1 if it is odd.
  int n = LU.rows;
  int i;
  //Find det(P)
  int perm_count = 0;
  for(i=0;i<n;i++){
    if(p[i] != i){
      perm_count++;
    }
  }
  int sign = (perm_count & 1 ? -1 : 1);
  //find det(U) (det(L) = 1)
  double det = matrix_ref(LU,0,0);
  for(i=1;i<n;i++){
    det *= matrix_ref(LU,i,i);
  }
  return det * sign;
}
//for now this stores A^-1^t in B
dmatrix lup_invert(dmatrix LU, int *p){
  int n = LU.rows;
  double y[n];
  dmatrix B = identity_dmatrix(n);
  int i,j,k;
  for(k=0;k<n;k++){
    //Solve Ly = P*B[k]
    for(i=0;i<n;i++){
      double sum = 0.0;
      for(j=0;j<i-1;j++){
        sum += matrix_ref(LU,i,j)*y[j];
      }
      y[i] = matrix_ref(B,k,p[j]) - sum;
    }
    //Solve Ux = y, x is stored in B[k]
    for(i=0;i<n;i++){
      double sum = 0.0;
      for(j=i;j<n;j++){
        sum += matrix_ref(LU,i,j)*matrix_ref(B,k,j);
      }
      matrix_ref(B,k,i) = (y[i] - sum)/matrix_ref(LU,i,i);
    }
  }
  square_transpose(B);
  return B;
}
/*
  LU decomposition

  //LU decomposition of A, an nxn matrix
  If A is a 1x1 matrix L = 1 and U = A
  otherwise recursively compute L and U via the following:
  Decompose A into a 2x2 block matrix
  Let A = |a = A[0][0]   | w^t = A[0][1-n] |
          |v = A[1-n][0] | A' = A[1-n][1-n]|
  This is equivlent to the product of lower and upper triangluar block matrices
  |1   | zero(n-1)[0] | | a              | w^t           |
  |v/a | I(n-1)       | | zero(n-1)[n-1] | A' - (v*w^t)/a |
  The (n-1)x(n-1) matrix A' - (v*w^t)/a is called the
  Schur complement of A wrt A[1][1] (v*w^t is the outer product of v and w)

  Now let A' - (v*w^t)/a = L'U', then A can be defined as
  | 1   | 0 | |a | w^t|
  | v/a | L'| |0 | U' |

  Block LU decomp
  |A|B|= |I    |0|*|A|0       |*|I|A^-1B|
  |C|D|  |CA^-1|0| |0|D-CA^-1B| |0|I    |
  L = [[I,0],[CA^-1,0]]
  U = [[A,A^-i*B],[0,D-CA^-1B]]

*/
//Pure LU decomposition, no permutations
void LU_decomp(double *A, int n, int stride){
  int i,j,k;
  //each iteration of the outer loop is one step in the recursive procedure
  for(k=0;k<n;k++){
    double a = A[k*stride +k];
    for(i=k+1;i<n;i++){
      A[i*stride + k] /= a; //A[i-n][k] = v/a
      for(j=k+1;j<n;j++){
        //Set A[1-n][1-n] to the schur complement of A wrt a.
        A[i*stride + j] -= A[i*stride + k] * A[k*stride + j];
      }
    }
  }
  return;
}
void LU_decomp_nonsquare(double *A, int rows, int cols, int stride){
  int M = rows, N = cols;
  int i,j,k;
  for(k=0;k<N;k++){
    double a = A[k*stride +k];
    for(i=k+1;i<M;i++){
      A[i*stride + k] /= a; //A[i-n][k] = v/a
      for(j=k+1;j<N;j++){
        A[i*stride + j] -= A[i*stride + k] * A[k*stride + j];
      }
    }
  }
  return;
}
/* void LUP_decomp_lapack(double *A, int n, int stride, int *pi){ */
/*   LAPACKE_dgetrf(LAPACKE_ROW_MAJOR, n, n, A, stride, pi); */
/* } */
//Compute the LUP decompisition of A, LU computed inplace
//P is store in pi such that P[i][j] = 1 if pi[i] = j, otherwise p[i][j] = 0
void LUP_decomp(double *A, int n, int stride, int *pi, int *signum){
  int i,j,k;
  *signum = 1;
  for(i=0;i<n;i++){
    pi[i] = i;
  }
  //We should determine the pivot based on the normalized values
  //(i.e divide each row by the maximum value in that row before comparing)
  for(k=0;k<n;k++){
    //find the pivot
    int p = cblas_idamax(n-k, A+k*stride, stride);
    //swap rows
    if(p != k){
      *signum = -(*signum);
      ARR_SWAP(pi,p,k);
      cblas_dswap(n, A + k*stride, 1, A + p*stride, 1);
    }
    //the rest is the same
    double a = A[k*stride + k];
    for(i=k+1;i<n;i++){
      A[i*stride + k] /= a; //A[i-n][k] = v/a
      for(j=k+1;j<n;j++){
        //Set A[1-n][1-n] to the schur complement of A wrt a.
        A[i*stride + j] = A[i*stride + k]*A[k*stride + j];
      }
    }
  }
}
void LDL_decomp_lapack(double *A, int n, int stride, int *pivot){
  LAPACKE_dsytrf(LAPACK_ROW_MAJOR, 'U', n, A, stride, pivot);
}
void LDL_decomp(double *A, int n, int stride){
  /*
    D[j] = A[j, j] - sum(k=0,j-1,L[j,k]^2*D[k])
    L[i,j] = 1/D[j]*(A[i,j] * sum(k=0,j-1,L[i,k]*L[j,k]*D[k])
   */
  int i,j,k;
  double v[n];
  for(j=0;j<n;j++){
    for(i=0;i<j;i++){
      //D[j] = A[j,j] - sum(k=0,j-1,L[j,k]^2 *D[k])
      A[j*stride + j] -= A[j*stride + i]*A[j*stride + i]*A[i*stride + i];
    }
    for(i=j+1;i<n;i++){
      double tmp = 0.0;
      for(k=0;k<j;k++){
        //sum(k=0,j-1,L[i,k]*L[j,k]*D[k]
        tmp += A[i*stride + k]*A[j*stride + k]*A[k*stride + k];
      }
      tmp = (A[i*stride+j] - tmp)/A[i*stride + i];
      //L[i,j] = (A[i,j] - sum(k=0,j-1,L[i,k]*L[j,k]*D[k]))/D[j]
      A[i*stride + j] = tmp;
    }
  }
}
/*
  L[j,j] = sqrt(A[j,j] - sum(k=0,k<j-1,L[j,k]*L[j,k]))
  L[i,j] = (A[i,j] - sum(k=0,k<j-1,L[i,k]*L[j,k]))/L[j,j] for i>j
*/
void cholesky_decomp(double *A, int n, int stride){
  int i,j,k;
  double sum;
  for(i=0;i<n;i++){
    for(j=0;j<i;j++){
      sum = cblas_ddot(j-1, A+i*stride, 1, A+j*stride, 1);
      A[i*stride + j] = (A[i*stride + j] - sum)/A[j*stride + j];
    }
    sum = cblas_ddot(j-1, A+i*stride, 1, A+i*stride, 1);
    A[i*stride + i] = sqrt(A[j,j] - sum);
  }
}
void cholesky_decomp_lapack(double *A, int n, int stride){
  LAPACKE_dpotrf(LAPACK_ROW_MAJOR, 'l', n, A, stride);
}
void cholesky_decomp_complex(complex_double *A, int n, int stride){
  int i,j,k;
  complex_double sum;
  for(i=0;i<n;i++){
    for(j=0;j<i;j++){
      sum = cblas_zdotc(j-1, (void*)(A + i*stride), 1, (void*)(A + j*stride), 1);
      A[i*stride + j] = (A[i*stride + j] - sum)/A[j*stride + j];
    }
    sum = cblas_zdotc(j-1, (void*)(A + i*stride), 1, (void*)(A + i*stride), 1);
    A[i*stride + i] = csqrt(A[i*stride + i] - sum);
  }
}
//Given x calc
void gen_householder_vector(const double *x, int n, int incx,
                            double *v, double *beta){
  //v = (x - ||x||_2)/||x_2||
  double sigma = cblas_ddot(n-1,x+incx,incx,x+incx,incx);
  cblas_dcopy(n-1, x+incx,incx,v+1,1);
  if(sigma == 0){
    v[0] = 1;
    *beta = (x[0] >= 0 ? 0 : -2);
  } else {
    double mu = sqrt(x[0]*x[0] + sigma);
    if(x[0] <= 0){
      v[0] = x[0] - mu;
    } else {
      v[0] = -sigma/(x[0]+mu);
    }
    *beta = (2*v[0]*v[0])/(sigma + v[0]*v[0]);
    cblas_dscal(n, 1.0/v[0], v, 1);
    //In theory this should be true already, but due to floating point
    //rounding it may not be.
    v[0] = 1.0;
  }
}

void gen_householder_vector_complex(const complex_double *x, int n, int incx,
                                    complex_double *v, complex_double *beta){
  //I'm not totally sure how to to this, so this should work, but might not
  //be the most numerically stable way to do it
  //v = (x +/- exp(i*arg(x[0]))*||x||_2)/
  /* complex_double sigma, arg_x, alpha; */
  /* sigma = cblas_zdotc(n-1,x+incx,incx,x+incx,incx); */
  /* arg_x = carg(x[0]); */
  /* alpha = cexp(I * arg_x); */

  /* cblas_zcopy(n-1, x+incx,incx,v+1,1); */
  /* //This could also be + alpha, one will be more numerically stable depending */
  /* //on the value of the rest of x */
  /* v[0] = x[0] - alpha; */

  /* *beta = (2*v[0]*v[0])/(sigma + v[0]*v[0]); */
  /* //normalize */
  /* cblas_zscal(n, 1.0/v[0], v, 1); */
}
void gen_givens_rotation(double a, double b, double *c, double *s){
  double tau;
  if(fabs(b) == 0.0){
    *c = 1;
    *s = 0;
  } else {
    if(fabs(b) > fabs(a)){
      tau = -a/b;
      *s = 1/sqrt(1 + (tau*tau));
      *c = *s*tau;
    } else {
      tau = -b/a;
      *c = 1/sqrt(1 + (tau*tau));
      *s = *c*tau;
    }
  }
}
//m is unused, but in the argument list for consistancy
void apply_row_givens_rotation(double *A, int m, int n, int stride,
                               int i, int k, double c, double s){
  int j;
  double *A_i = A + i*stride;
  double *A_k = A + k*stride;
  for(j=0;j<n;j++){
    double tau_1 = A_i[j];
    double tau_2 = A_k[j];
    A_i[j] = c*tau_1 - s*tau_2;
    A_k[j] = s*tau_1 + c*tau_2;
  }
}
void apply_col_givens_rotation(double *A, int m, int n, int stride,
                               int i, int k, double c, double s){
  int j;
  double *A_i = A + i;
  double *A_k = A + k;
  for(j=0;j<n;j++){
    double tau_1 = A_i[j*stride];
    double tau_2 = A_k[j*stride];
    A_i[j*stride] = c*tau_1 - s*tau_2;
    A_k[j*stride] = s*tau_1 + c*tau_2;
  }
}

void apply_householder_vector(double *A, int m, int n, int stride,
                              double *v, double beta){
  //A = (I - beta*v*v^T)*A = A - (beta*v)(v^T*A)
  //                              mx1     1xm.mxn

  //(v^TA)^T = A^T*v^T  (nx1, stored the same as 1xn however)
  double v_t[n];
  cblas_dgemv(CblasRowMajor, CblasTrans, m, n, 1.0, A, stride,
              v, 1, 0.0, v_t, 1);
  //A = A - beta*v -
  cblas_dger(CblasRowMajor, m, n, -beta, v, 1, v_t, 1, A, stride);
}
void apply_complex_householder_vector(complex_double *A, int m, int n, int stride,
                                      complex_double *v, complex_double beta){
  //A = (I - beta*v*v^T)*A = A - (beta*v)(v^T*A)
  //                              mx1     1xm.mxn

  //(v^HA)^H = A^H*v  (nx1, stored the same as 1xn however)
  //I'm not sure if these should be hermitian transposes
  complex_double v_t[n];
  cblas_zgemv(CblasRowMajor, CblasConjTrans, m, n,
              (double*)&cmplx_1, (double*)A, stride,
              (double*)v, 1, (double*)&cmplx_0, (double*)v_t, 1);
  //A = A - beta*v
  beta *= -1.0;
  cblas_zgerc(CblasRowMajor, m, n, (double*)&beta, (double*)v,
              1, (double*)v_t, 1, (double*)A, stride);
}

//There's a lot of room for optimization here
void householder_qr(double *A, int m, int n, int stride){
  int i;
  double v[n];
  double beta;
  //There must be a way to do this in a more row major friendly way
  for(i = 0;i < n; i++){
    gen_householder_vector(A + i*stride + i, m - i, stride, v, &beta);
    apply_householder_vector(A, m-i, n-i, stride, v, beta);
    if(i<m){//only happens when n == m, this should be moved out of the loop
      cblas_dcopy(m-(i+1), A+i, stride, v+1, 1);
    }
  }
}
//identical to qr reduction, but stop one with one subdiagonal left
void hessenberg_reduction_householder(double *A, int n, int m, int stride){
  int i;
  double v[n];
  double beta;
  for(i=0;i<n-2;i++){
    gen_householder_vector(A + i*stride + i, m - i, stride, v, &beta);
    apply_householder_vector(A, m-i, n-i, stride, v, beta);
    if(i<m){//only happens when n == m, this should be moved out of the loop
      cblas_dcopy(m-(i+1), A+i, stride, v+1, 1);
    }
  }
}    
//Compute the least squares solution to Ax=b, A is overwritten by its QR
//factorization, b is overwritten by the solution, and the minimum residual
//is returned
double householder_qr_lls(double *A, int m, int n, int stride,
                          double *b, int incb, double *x, int incx){
  householder_qr(A,m,n,stride);
  int i;
  double v[m];
  double beta;
  //the leading 1 in the householder vectors stored in A is omitted, so
  //We need to set it explicitly
  v[0] = 1.0;

  //Compute Q^T*b, using the householder vectors stored in A
  for(i=0;i<n;i++){
    //Copy the ith householder vector into v
    int len = m - i;
    cblas_dcopy(len-1, A + i, stride, v+1, 1);
    double beta = 2/cblas_ddot(len, v, 1, v, 1);
    double alpha = beta * cblas_ddot(len, v, 1, b + i, 1);
    //b[i:m] -= beta*(v^Tb[j:m])v
    cblas_daxpy(len, -alpha, v, 1, b+i, 1);
  }
  //Solve R[0:n,0:n]*x_ls = b[1:n]
  //Not sure if this is unit diagonal or not, so erring on the side
  //of caution, if it is unit change this
  cblas_dtrsv(CblasRowMajor, CblasUpper, CblasNoTrans, CblasNonUnit,
              n, A, stride, b, 1);
  //The minimum residual is given by
  //rho = cblas_dnrm2(m-n, b+n, 1);
  return cblas_dnrm2(m-n, b+n, 1);
}

void givens_qr(double *A, int m, int n, int stride){
  int i,j;
  double c,s;
  for(i=1;i<m;i++){
    for(j=0;j<i;j++){
      gen_givens_rotation(A[j*stride + j],A[i*stride + j], &c, &s);
      apply_row_givens_rotation(A,m,n,stride,
                                j,i,c,s);
    }
  }
}
/* void block_qr(double *A, int m, int n, int stride){ */
/*   if(n < threshold){ */
/*     //normal qr */
/*   } else { */
/*     n1 = n/2; */

//QR decomposition, A is overwritten by Q, R is stored in seperate array
//There's probably a better way to do this for row major storage
void modified_gram_schmidt(double *A,int m, int n, int a_stride,
                           double *R, int r_stride){
  //Assume R is allocated
  int k,j;
  double R_kk, R_kj;
  if(m > n){
    //Orthagonalize based on the column space (this is the normal way to do this)
    for(k=0;k<n;k++){
      R_kk = cblas_dnrm2(m, A + k, a_stride);
      R[k + k*r_stride] = R_kk;
      //Q[1:m,k] = A[1:m,k]/R[k,k]
      cblas_dscal(m, 1/R_kk, A + k, a_stride);
      for(j=k+1;j<n;j++){
        //R[k,j] = Q[1:m,k]^T * A[1:m,j]
        R_kj = cblas_ddot(m, A + k, a_stride, A + j, a_stride);
        R[k + j*r_stride] = R_kj;
        //A[1:m,j] -= Q[1:m,k]*R_kj
        cblas_daxpy(m, -R_kj, A+k, a_stride, A+j, a_stride);
      }
    }
  } else {
    //orthagonalize based on the row space, this might not work, I've just
    //sort of extrapolated the column version to work on rows
    for(k=0;k<m;k++){
      R_kk = cblas_dnrm2(n, A + k*a_stride, 1);
      R[k + k*r_stride] = R_kk;
      cblas_dscal(n, 1/R_kk, A + k*a_stride, 1);
      for(j=k+1;j<m;j++){
        //R[k,j] = Q[k,1:n]^T * A[j,1:n]
        R_kj = cblas_ddot(n, A + k*a_stride, 1, A + j*a_stride, 1);
        R[k + j*r_stride] = R_kj;
        cblas_daxpy(n, -R_kj, A+k*a_stride, 1, A+j*a_stride, 1);
      }
    }
  }
}
