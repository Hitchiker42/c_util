#ifndef __LINALG_H__
#define __LINALG_H__
#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>
/*
  The layout/idea for implementing matrices like this comes from gsl
*/
typedef struct dblock dblock;
typedef struct dvector dvector;
typedef struct dmatrix dmatrix;
struct dblock {
  double *ptr;
  size_t sz;
};
struct dmatrix {
  double *mat;
  size_t rows;
  size_t cols;
  size_t stride;
  //can be NULL, if the lsb of the pointer is set then this matrix
  //owns the block and should free it when the matrix is freed
  dblock *block;
};
struct dvector {
  double *vec;
  size_t len;
  size_t stride;
  dblock *block;
};
#define matrix_ref(m,i,j) (m.mat[(i*m.stride) + j])
#define matrix_swap(m,i0,j0,i1,j1)                       \
  ARR_SWAP(m.mat,(i0*m.stride + j0),(i1*m.stride + j0))
#define _matrix_column(m,i,allocate)                    \
  ({double col = allocate(m.rows * sizeof(double));     \
    int j;                                              \
    for(j=0;j<m.cols;j++){                              \
      col[j] = matrix_ref(m,j,i);                       \
    }                                                   \
    col;})
#define matrix_column_tmp(m,i) _matrix_column(m,i,alloca)
#define matrix_column(m,i) _matrix_column(m,i,malloc)
#ifdef __cplusplus
}
#endif
#endif /* __LINALG_H__ */
