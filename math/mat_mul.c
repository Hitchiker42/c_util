#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#define IN_MAT_MUL
#include "mat_mul_simd.c"
/*
  Simple testing shows that for multiplying 2048x2048 matrices
  block sizes are ordered:  32 < 16 << 64 < 8
*/
#define mat_mul_recursion_threshold 32
/*
  There might be some errors from when I switched the order of M and N.
*/
//C = A*B       
double* mat_mul_naive(size_t M, size_t N, size_t P,
                      const double* restrict A, size_t A_stride,
                      const double* restrict B, size_t B_stride,
                            double* restrict C, size_t C_stride){
  //AB[i,j] = A[i,k]*B[k,j];
  int i,j,k;
  for(i=0;i<M;i++){
    for(j=0;j<P;j++){
      double sum = 0;
      for(k=0;k<N;k++){
        sum += A[i*A_stride + k]*B[k*B_stride + j];
      }
      C[i*C_stride + j] = sum;
    }
  }
  return C;
}
//C = A*B + C 
double* mat_fma_naive(size_t M, size_t N, size_t P,
                      const double* restrict A, size_t A_stride,
                      const double* restrict B, size_t B_stride,
                            double* restrict C, size_t C_stride){
  int i,j,k;
  for(i=0;i<M;i++){
    for(j=0;j<P;j++){
      double sum = 0;
      for(k=0;k<N;k++){
        C[i*C_stride + j] += A[i*A_stride + k]*B[k*B_stride + j];
      }
    }
  }
  return C;
}
//N needs to be divisible by this

double *sq_mat_mul_recursive(size_t N,
                             const double *restrict A, size_t A_stride,
                             const double *restrict B, size_t B_stride,
                             //C needs to be zeroed before calling
                                   double *restrict C, size_t C_stride){
  if(N < mat_mul_recursion_threshold){
    //Compare this to using the optimized routines for small matrices
    return mat_fma_naive(N,N,N,A,A_stride,B,B_stride,C,C_stride);
  }
  /*
    View A,B,C as 4x4 block matrices (with n/2xn/2 sized entries) then:
    C = |A00*B00 + A01*B10|A00*B01 + A01*B11|
        |A10*B00 + A11*B10|A10*B01 + A11*B11|
  */
  size_t N_2 = N/2;//Need to deal with cases when N is odd

  const double *A_blocks[4] = {A, A + N_2, 
                               A + (N_2 * A_stride), A + (N_2 * A_stride + N_2)};
  const double *B_blocks[4] = {B, B + N_2,
                               B + (N_2 * B_stride), B + (N_2 * B_stride + N_2)};
  double *C_blocks[4] = {C, C + (N_2 * C_stride),
                         C + N_2, C + (N_2 * C_stride + N_2)};
  //fork threads
  sq_mat_mul_recursive(N_2, //A00*B00 (C00)
                       A_blocks[0], A_stride,
                       B_blocks[0], B_stride,
                       C_blocks[0], C_stride);
  sq_mat_mul_recursive(N_2, //A00*B01 (C01)
                       A_blocks[0], A_stride,
                       B_blocks[1], B_stride,
                       C_blocks[1], C_stride);
  sq_mat_mul_recursive(N_2, //A10*B00 (C10)
                       A_blocks[2], A_stride,
                       B_blocks[0], B_stride,
                       C_blocks[2], C_stride);
  sq_mat_mul_recursive(N_2, //A10*B01 (C11)
                       A_blocks[2], A_stride,
                       B_blocks[1], B_stride,
                       C_blocks[3], C_stride);
  //sync threads
  sq_mat_mul_recursive(N_2, //A01*B10 (C00)
                       A_blocks[1], A_stride,
                       B_blocks[2], B_stride,
                       C_blocks[0], C_stride);
  sq_mat_mul_recursive(N_2, //A01*B11 (C01)
                       A_blocks[1], A_stride,
                       B_blocks[3], B_stride,
                       C_blocks[1], C_stride);
  sq_mat_mul_recursive(N_2, //A11*B10 (C10)
                       A_blocks[3], A_stride,
                       B_blocks[2], B_stride,
                       C_blocks[2], C_stride);
  sq_mat_mul_recursive(N_2, //A11*B11 (C11)
                       A_blocks[3], A_stride,
                       B_blocks[3], B_stride,
                       C_blocks[3], C_stride);
  return C;
}
#define MAX(a,b) (a > b ? a : b)
double* mat_mul_recursive(size_t M, size_t N, size_t P,
                          const double* restrict A, size_t A_stride,
                          const double* restrict B, size_t B_stride,
                                double* restrict C, size_t C_stride){
  /*
    3 Cases based on which of M,N,P is the largest:
    MAX(M,N,P) = M, split A horizontally
    C = |A0|*B = |A0*B|
        |A1|     |A1*B|
    MAX(M,N,P) = P, split B vertically
    C = A*|B0|B1| = |A*B0|A*B1|
    MAX(M,N,P) = N, split both (A vertically, B horizontally),
      this is just a normal matrix multiplication.
    C = |A0|A1|*|B0| = A0*B0  + A1*B1
                |B1|
  */
  size_t max = MAX(N,MAX(M,P));
  if(max < mat_mul_recursion_threshold){
    return mat_fma_naive(M, N, P,
                         A, A_stride,
                         B, B_stride,
                         C, C_stride);
  } else {
    if(max == M){
      size_t M_2 = M/2;
      size_t M_21 = (M+1)/2;
      mat_mul_recursive(M_2,N,P,
                        A,A_stride,
                        B,B_stride,
                        C,C_stride);
      mat_mul_recursive(M_21,N,P,
                        A+(M_2*A_stride),A_stride,
                        B,B_stride,
                        C+(M_2*C_stride),C_stride);
    } else if(max == P){
      size_t P_2 = P/2;
      size_t P_21 = (P+1)/2;
      mat_mul_recursive(M,N,P_2,
                        A,A_stride,
                        B,B_stride,
                        C,C_stride);
      mat_mul_recursive(M,N,P_21,
                        A,A_stride,
                        B+P_2,B_stride,
                        C+P_2,C_stride);
    } else {
      size_t N_2 = N/2;
      size_t N_21 = (N+1)/2;
      mat_mul_recursive(M,N_2,P,
                        A,A_stride,
                        B,B_stride,
                        C,C_stride);
      mat_mul_recursive(M,N_21,P,
                        A + N_2,A_stride,
                        B + (N_2*B_stride),B_stride,
                        C,C_stride);
    }
  }
  return C;
}
