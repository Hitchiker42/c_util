#ifndef __MAT_MUL_H__
#define __MAT_MUL_H__
#ifdef __cplusplus
extern "C" {
#endif
double* mat_mul_naive(size_t M, size_t N, size_t P,
                      const double* restrict A, size_t A_stride,
                      const double* restrict B, size_t B_stride,
                            double* restrict C, size_t C_stride);
double* mat_fma_naive(size_t M, size_t N, size_t P,
                      const double* restrict A, size_t A_stride,
                      const double* restrict B, size_t B_stride,
                            double* restrict C, size_t C_stride);
double *sq_mat_mul_recursive(size_t N,
                             const double *restrict A, size_t A_stride,
                             const double *restrict B, size_t B_stride,
                                   double *restrict C, size_t C_stride);
double* mat_mul_recursive(size_t M, size_t N, size_t P,
                          const double* restrict A, size_t A_stride,
                          const double* restrict B, size_t B_stride,
                                double* restrict C, size_t C_stride);
#ifdef __cplusplus
}
#endif
#endif /* __MAT_MUL_H__ */
