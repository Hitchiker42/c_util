#if (defined IN_MAT_MUL)
#include <x86intrin.h>

//Manually optimized routines for multiplying small square matrices

//Optimized 4x4 matrix multiplication (for doubles)
#if (defined __AVX__)
void dmat4_fma(double* restrict A, size_t A_stride,
               double* restrict B, size_t B_stride,
               double* restrict C, size_t C_stride){
  //these are just a convenience, they shouldn't actuall exist at runtime
  double *A_rows[4] = {A + A_stride*0,
                       A + A_stride*1,
                       A + A_stride*2,
                       A + A_stride*3};
  double *B_rows[4] = {B + B_stride*0,
                       B + B_stride*1,
                       B + B_stride*2,
                       B + B_stride*3};
  double *C_rows[4] = {C + C_stride*0,
                       C + C_stride*1,
                       C + C_stride*2,
                       C + C_stride*3};

  __m256d r[4];
  __m256d c[4];
  __m256d dst[4];
  __m256d rc[4];
  int i;

  r[0] = _mm256_load_pd(A_rows[0]);
  r[1] = _mm256_load_pd(A_rows[1]);
  r[2] = _mm256_load_pd(A_rows[2]);
  r[3] = _mm256_load_pd(A_rows[3]);

  dst[0] = _mm256_load_pd(C_rows[0]);
  dst[1] = _mm256_load_pd(C_rows[1]);
  dst[2] = _mm256_load_pd(C_rows[2]);
  dst[3] = _mm256_load_pd(C_rows[3]);

  c[0] = _mm256_set_pd(B_rows[0][0], B_rows[1][0],
                       B_rows[2][0], B_rows[3][0]);
  c[1] = _mm256_set_pd(B_rows[0][1], B_rows[1][1],
                       B_rows[2][1], B_rows[3][1]);
  c[2] = _mm256_set_pd(B_rows[0][2], B_rows[1][2],
                       B_rows[2][2], B_rows[3][2]);
  c[3] = _mm256_set_pd(B_rows[0][3], B_rows[1][3],
                       B_rows[2][3], B_rows[3][3]);
  
  for(i=0;i<4;i++){
    
    //Multiplactions for row i of dest
    rc[0] = _mm256_mul_pd(r[i],c[0]);
    rc[1] = _mm256_mul_pd(r[i],c[1]);
    rc[2] = _mm256_mul_pd(r[i],c[2]);
    rc[3] = _mm256_mul_pd(r[i],c[3]);
    //additions for row i, using hadd:
    //hadd(|a|b|c|d|,|e|f|g|h) -> |a+b|c+d|e+f|g+h|
    rc[0] = _mm256_hadd_pd(rc[0], rc[1]);
    rc[2] = _mm256_hadd_pd(rc[2], rc[3]);
    
    rc[0] = _mm256_hadd_pd(rc[0], rc[2]);
    //rc0 = |sum(rc0)|sum(rc1)|sum(rc2)|sum(rc3)|

    rc[0] = _mm256_add_pd(dst[i], rc[0]);
    _mm256_store_pd(C_rows[i], rc[0]);
  }
}
#elif (defined __SSE3__)
void dmat4_fma(double* restrict A, size_t A_stride,
               double* restrict B, size_t B_stride,
               double* restrict C, size_t C_stride){
  
  double *A_rows[4] = {A + A_stride*0,
                       A + A_stride*1,
                       A + A_stride*2,
                       A + A_stride*3};
  double *B_rows[4] = {B + B_stride*0,
                       B + B_stride*1,
                       B + B_stride*2,
                       B + B_stride*3};
  double *C_rows[4] = {C + C_stride*0,
                       C + C_stride*1,
                       C + C_stride*2,
                       C + C_stride*3};
  __m128d r[4];
  __m128d c[4];
  __m128d dst[4];
  __m128d rc[4];
  int j;
  //We can only have enough registers to do 2 rows at a time
  for(j=0;j<2;j++){
    double* tmp = A + 8*j;
    r[0] = _mm_load_pd(A_rows[2*j]);
    r[1] = _mm_load_pd(A_rows[2*j] + 2);
    r[2] = _mm_load_pd(A_rows[(2*j)+1]);
    r[3] = _mm_load_pd(A_rows[(2*j)+1] + 2);
    
    dst[0] = _mm_load_pd(C_rows[2*j]);
    dst[1] = _mm_load_pd(C_rows[2*j] + 2);
    dst[2] = _mm_load_pd(C_rows[(2*j)+1]);
    dst[3] = _mm_load_pd(C_rows[(2*j)+1] + 2);

    tmp = B + 2*j;
    c[0] = _mm_set_pd(tmp[0], tmp[4]);
    c[1] = _mm_set_pd(tmp[8], tmp[12]);
    c[2] = _mm_set_pd(tmp[1], tmp[5]);
    c[3] = _mm_set_pd(tmp[9], tmp[13]);
    int i;
    for(i=0;i<4;i++){
      rc[0] = _mm_mul_pd(r[i],c[0]);
      rc[1] = _mm_mul_pd(r[i],c[1]);
      rc[2] = _mm_mul_pd(r[i],c[2]);
      rc[3] = _mm_mul_pd(r[i],c[3]);
      //additions for row i, using hadd:
      //hadd(|a|b|c|d|,|e|f|g|h) -> |a+b|c+d|e+f|g+h|
      rc[0] = _mm_hadd_pd(rc[0], rc[1]);
      rc[2] = _mm_hadd_pd(rc[2], rc[3]);
    
      rc[0] = _mm_hadd_pd(rc[0], rc[2]);
      //rc0 = |sum(rc0)|sum(rc1)|sum(rc2)|sum(rc3)|
      rc[0] = _mm_add_pd(dst[i], rc[0]);
      _mm_store_pd(C_rows[i], rc[0]);
    }
  }
}
#else
void __attribute__((optimize("unroll-loops")))
dmat4_fma(double* restrict A, size_t A_stride,
          double* restrict B, size_t B_stride,
          double* restrict C, size_t C_stride){
  int i,j;
  for(i=0;i<4;i++){
    for(j=0;j<4;j++){
      C[i*C_stride + j] += A[i*A_stride + 0]*B[0*B_stride + j];
      C[i*C_stride + j] += A[i*A_stride + 1]*B[1*B_stride + j];
      C[i*C_stride + j] += A[i*A_stride + 2]*B[2*B_stride + j];
      C[i*C_stride + j] += A[i*A_stride + 3]*B[3*B_stride + j];
    }
  }
}
#endif

//Optimized 8x8 matrix multiplication (for doubles)
#if (defined __AVX__)
/*
  View A,B,C as 4x4 block matrices (with n/2xn/2 sized entries) then:
  C = |A00*B00 + A01*B10|A00*B01 + A01*B11|
  |A10*B00 + A11*B10|A10*B01 + A11*B11|
  //We have 8 operations, we want to order them so that we minimize loads,
  //Loading B is a lot more work than loading A, so we want to minimize
  //the loads of B first, then if possible the loads of A, giving this order:

  C10 += A10*B00 -> C00 += A00*B00 -> C01 += A00*B01 -> C11 += A10*B01
  C10 += A11*B10 -> C00 += A01*B10 -> C01 += A01*B11 -> C11 += A11*B11

  This only really makes sense for AVX, since sse registers can't hold
  a full 4x4 matrix
*/
void dmat8_fma(double* restrict A, size_t A_stride,
               double* restrict B, size_t B_stride,
               double* restrict C, size_t C_stride){
  double *A_rows[8] = {A + A_stride * 0,
                       A + A_stride * 1,
                       A + A_stride * 2,
                       A + A_stride * 3,
                       A + A_stride * 4,
                       A + A_stride * 5,
                       A + A_stride * 6,
                       A + A_stride * 7};
  double *B_rows[8] = {B + B_stride * 0,
                       B + B_stride * 1,
                       B + B_stride * 2,
                       B + B_stride * 3,
                       B + B_stride * 4,
                       B + B_stride * 5,
                       B + B_stride * 6,
                       B + B_stride * 7};
  double *C_rows[8] = {C + C_stride * 0,
                       C + C_stride * 1,
                       C + C_stride * 2,
                       C + C_stride * 3,
                       C + C_stride * 4,
                       C + C_stride * 5,
                       C + C_stride * 6,
                       C + C_stride * 7};
  double *A_00[4] = {A_rows[0],  A_rows[1],  A_rows[2],  A_rows[3]};
  double *A_01[4] = {A_rows[0]+4,A_rows[1]+4,A_rows[2]+4,A_rows[3]+4};
  double *A_10[4] = {A_rows[4],  A_rows[5],  A_rows[6],  A_rows[7]};
  double *A_11[4] = {A_rows[4]+4,A_rows[5]+4,A_rows[6]+4,A_rows[7]+4};

  double *C_00[4] = {C_rows[0],  C_rows[1],  C_rows[2],  C_rows[3]};
  double *C_01[4] = {C_rows[0]+4,C_rows[1]+4,C_rows[2]+4,C_rows[3]+4};
  double *C_10[4] = {C_rows[4],  C_rows[5],  C_rows[6],  C_rows[7]};
  double *C_11[4] = {C_rows[4]+4,C_rows[5]+4,C_rows[6]+4,C_rows[7]+4};
  
  __m256d a[4];
  __m256d b[4];
  __m256d c[4];
  __m256d ab[4];
  int i;
#define do_mult(dst)                            \
  for(i=0;i<4;i++){                             \
    ab[0] = _mm256_mul_pd(a[i],b[0]);           \
    ab[1] = _mm256_mul_pd(a[i],b[1]);           \
    ab[2] = _mm256_mul_pd(a[i],b[2]);           \
    ab[3] = _mm256_mul_pd(a[i],b[3]);           \
    ab[0] = _mm256_hadd_pd(ab[0], ab[1]);       \
    ab[2] = _mm256_hadd_pd(ab[2], ab[3]);       \
    ab[0] = _mm256_hadd_pd(ab[0], ab[2]);       \
    ab[0] = _mm256_add_pd(c[i], ab[0]);         \
    _mm256_store_pd(dst[i], ab[0]);             \
  }
  //A10,B00,C10
  for(i=0;i<4;i++){
    a[i] = _mm256_load_pd(A_10[i]);
    b[i] = _mm256_set_pd(B_rows[0][i], B_rows[1][i],
                         B_rows[2][i], B_rows[3][i]);
    c[i] = _mm256_load_pd(C_10[i]);
  }
  
  do_mult(C_10);
  
  //A00,C00
  for(i=0;i<4;i++){
    a[i] = _mm256_load_pd(A_00[i]);
    c[i] = _mm256_load_pd(C_00[i]);
  }
  
  do_mult(C_00);

  //B01,C01
  for(i=0;i<4;i++){
    b[i] = _mm256_set_pd(B_rows[0][i+4], B_rows[1][i+4],
                         B_rows[2][i+4], B_rows[3][i+4]);
    c[i] = _mm256_load_pd(C_01[i]);
  }

  do_mult(C_01);

  //A10,C11
  for(i=0;i<4;i++){
    a[i] = _mm256_load_pd(A_10[i]);
    c[i] = _mm256_load_pd(C_11[i]);
  }

  do_mult(C_11);

  //A11,B10,C10
  for(i=0;i<4;i++){
    a[i] = _mm256_load_pd(A_11[i]);
    b[i] = _mm256_set_pd(B_rows[4][i], B_rows[5][i],
                         B_rows[6][i], B_rows[7][i]);
    c[i] = _mm256_load_pd(C_01[i]);
  }

  do_mult(C_10);

  //A01,C00
  for(i=0;i<4;i++){
    a[i] = _mm256_load_pd(A_01[i]);
    c[i] = _mm256_load_pd(C_00[i]);
  }

  do_mult(C_00);

  //B11,C01
  for(i=0;i<4;i++){
    b[i] = _mm256_set_pd(B_rows[4][i+4], B_rows[5][i+4],
                         B_rows[6][i+4], B_rows[7][i+4]);
    c[i] = _mm256_load_pd(C_01[i]);
  }

  do_mult(C_01);

  //A11,C11
  for(i=0;i<4;i++){
    a[i] = _mm256_load_pd(A_11[i]);
    c[i] = _mm256_load_pd(C_11[i]);
  }

  do_mult(C_11);
}
#else
void dmat8_fma(double* restrict A, size_t A_stride,
               double* restrict B, size_t B_stride,
               double* restrict C, size_t C_stride){
  double *A_blocks[4] = {A, A + 4, 
                         A + (4 * A_stride), A + (4 * A_stride + 4)};
  double *B_blocks[4] = {B, B + 4,
                         B + (4 * B_stride), B + (4 * B_stride + 4)};
  double *C_blocks[4] = {C, C + 4,
                         C + (4 * C_stride), C + (4 * C_stride + 4)};
  int i,j;
  /*
    C10 += A10*B00 -> C00 += A00*B00 -> C01 += A00*B01 -> C11 += A10*B01
    C10 += A11*B10 -> C00 += A01*B10 -> C01 += A01*B11 -> C11 += A11*B11
  */
  //C00 += A00*B00
  dmat4_fma(A_blocks[0], A_stride,
            B_blocks[0], B_stride,
            C_blocks[0], C_stride);
  //C01 += A00*B01
  dmat4_fma(A_blocks[0], A_stride,
            B_blocks[1], B_stride,
            C_blocks[1], C_stride);
  //C10 += A10*B00
  dmat4_fma(A_blocks[2], A_stride,
            B_blocks[0], B_stride,
            C_blocks[2], C_stride);
  //C11 += A10*B01
  dmat4_fma(A_blocks[2], A_stride,
            B_blocks[1], B_stride,
            C_blocks[3], C_stride);
  
  //C00 += A01*B10
  dmat4_fma(A_blocks[1], A_stride,
            B_blocks[2], B_stride,
            C_blocks[0], C_stride);
  
  //C01 += A01*B11
  dmat4_fma(A_blocks[1], A_stride,
            B_blocks[3], B_stride,
            C_blocks[1], C_stride);

  //C10 += A11*B10
  dmat4_fma(A_blocks[3], A_stride,
            B_blocks[2], B_stride,
            C_blocks[2], C_stride);
  //C11 += A11*B11
  dmat4_fma(A_blocks[3], A_stride,
            B_blocks[3], B_stride,
            C_blocks[3], C_stride);
  return;
}
#endif

//Optimized 16x16 matrix multiplication (for doubles)
void dmat16_fma_simd(double* restrict A, size_t A_stride,
                     double* restrict B, size_t B_stride,
                     double* restrict C, size_t C_stride){
  double *A_blocks[4] = {A, A + 8, 
                         A + (8 * A_stride), A + (8 * A_stride + 8)};
  double *B_blocks[4] = {B, B + 8,
                         B + (8 * B_stride), B + (8 * B_stride + 8)};
  double *C_blocks[4] = {C, C + 8,
                         C + (8 * C_stride), C + (8 * C_stride + 8)};
  //C00 += A00*B00
  dmat8_fma(A_blocks[0], A_stride,
            B_blocks[0], B_stride,
            C_blocks[0], C_stride);
  //C01 += A00*B01
  dmat8_fma(A_blocks[0], A_stride,
            B_blocks[1], B_stride,
            C_blocks[1], C_stride);
  //C10 += A10*B00
  dmat8_fma(A_blocks[2], A_stride,
            B_blocks[0], B_stride,
            C_blocks[2], C_stride);
  //C11 += A10*B01
  dmat8_fma(A_blocks[2], A_stride,
            B_blocks[1], B_stride,
            C_blocks[3], C_stride);
  
  //C00 += A01*B10
  dmat8_fma(A_blocks[1], A_stride,
            B_blocks[2], B_stride,
            C_blocks[0], C_stride);
  
  //C01 += A01*B11
  dmat8_fma(A_blocks[1], A_stride,
            B_blocks[3], B_stride,
            C_blocks[1], C_stride);

  //C10 += A11*B10
  dmat8_fma(A_blocks[3], A_stride,
            B_blocks[2], B_stride,
            C_blocks[2], C_stride);
  //C11 += A11*B11
  dmat8_fma(A_blocks[3], A_stride,
            B_blocks[3], B_stride,
            C_blocks[3], C_stride);
  return;
}
#endif
