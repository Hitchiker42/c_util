#include "C_util.h"
//tail recursive gcd
long gcd_simple(long a, long b){
  if(a == b){
    return a;
  } else if(a > b){
    return gcd_simple(a-b, b);
  } else {
    return gcd_simple(a, b-a);
  }
}

//Binary gcd
long gcd(long a, long b){
  if(a == 0){return b;}
  if(b == 0){return a;}
  int lz_a = __builtin_clzl(a), lz_b = __builtin_clzl(b);
  //Common factors of 2, we need to restore these at the end
  int shift = MIN(lz_a,lz_b);
  //remove factors of 2 from b
  b >>= lz_b;
  //loop until a is 0, swapping a and b as necessary to insure
  //a is the larger of the two. b will always be odd so we can
  //shift out the factors of 2 in a at the begining of each loop.
  while(1){
    a >>= lz_a;
    if(a < b){SWAP(a,b);}
    a -= b;
    if(a == 0){
      break;
    }
    lz_a = __builtin_clzl(a);
  }
  return (b << shift);
}
