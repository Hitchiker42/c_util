#ifndef __MATH_UTIL_H__
#define __MATH_UTIL_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "../C_util.h"

#include <complex.h>

#include <cblas.h>
#include <lapacke.h>

#include <gmp.h>
//gmp mpX_t types are defined as arrays, which causes some issues
//with things like swap.
typedef __mpz_struct *mpz_ptr;
typedef __mpz_struct mpz_struct;
typedef __mpq_struct *mpq_ptr;
typedef __mpq_struct mpq_struct;
typedef __mpf_struct *mpf_ptr;
typedef __mpf_struct mpf_struct;
//Using an explict complex type causes lots of type issues with
//blas functions witch just expect a pointer to a double/doubles for
//complex functions. Figure out how to fix this, preferably without
//having to write a bunch of explicit casts
typedef _Complex double complex_double;
#if !(defined(CMLPX))
/* Macros to expand into expression of specified complex type.  */
#define CMPLX(x, y) __builtin_complex ((double) (x), (double) (y))
#endif
static const complex_double cmplx_1 = CMPLX(1.0,0.0);
static const complex_double cmplx_0 = CMPLX(0.0,0.0);
static const complex_double cmplx_m1 = CMPLX(-1.0,0.0);

#ifdef __cplusplus
}
#endif
#endif /* __MATH_UTIL_H__ */
