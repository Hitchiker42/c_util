#ifndef __LINALG_H__
#define __LINALG_H__
#ifdef __cplusplus
extern "C" {
#endif
#include "math_util.h"
/*
  The layout/idea for implementing matrices like this comes from gsl
*/
typedef struct mem_block mem_block;
typedef struct dvector dvector;
typedef struct dmatrix dmatrix;
typedef struct c_dvector c_dvector;
typedef struct c_dmatrix c_dmatrix;
struct mem_block {
  void *ptr;
  size_t sz;
};
struct dmatrix {
  double *mat;
  size_t rows;
  size_t cols;
  size_t stride;
  //can be NULL, if the lsb of the pointer is set then this matrix
  //owns the block and should free it when the matrix is freed
  mem_block *block;
};
struct dvector {
  double *vec;
  size_t len;
  ssize_t stride;
  mem_block *block;
};
struct c_dmatrix {
  complex_double *mat;
  size_t rows;
  size_t cols;
  ssize_t stride;
  //can be NULL, if the lsb of the pointer is set then this matrix
  //owns the block and should free it when the matrix is freed
  mem_block *block;
};
struct c_dvector {
  complex_double *vec;
  size_t len;
  ssize_t stride;
  mem_block *block;
};
#define matrix_ref(m,i,j) (m.mat[(i*m.stride) + j])
#define matrix_swap(m,i0,j0,i1,j1)                       \
  ARR_SWAP(m.mat,(i0*m.stride + j0),(i1*m.stride + j0))
#define matrix_row(m,i)                         \
  ({dvector row = {0};                          \
    row.vec = m.mat + m.stride*i;               \
    row.len = m.cols;                           \
    row.stride = 1;                             \
    row;})
#define matrix_column(m,i)                      \
  ({dvector col = {0};                          \
    col.vec = m.mat + i;                        \
    col.len = m.rows;                           \
    col.stride = m.stride;                      \
    col;})
#define _matrix_column_transposee(m,i,allocate)         \
  ({double col = allocate(m.rows * sizeof(double));     \
    int j;                                              \
    for(j=0;j<m.cols;j++){                              \
      col[j] = matrix_ref(m,j,i);                       \
    }                                                   \
    col;})
#define matrix_column_transpose_tmp(m,i) _matrix_column(m,i,alloca)
#define matrix_column_transpose(m,i) _matrix_column(m,i,malloc)

double* double_matrix_copy(double *mat, int rows, int cols, int stride);
double* identity_matrix_double(int n);
double* random_double_vector(int n);
static dmatrix identity_dmatrix(int n){
  double *mem = identity_matrix_double(n);
  dmatrix ret = {mem, n, n, n};
  return ret;
}
static dmatrix dmatrix_copy(dmatrix A){
  double *mem = double_matrix_copy(A.mat,A.rows,A.cols,A.stride);
  dmatrix ret = {mem, A.rows, A.cols, A.stride};
  return ret;
}
//TODO: get rid of these (make sure to switch any uses to cblas first)
int argmax_d(double *V, int len, int stride, int abs);
static int dvector_argmax(dvector V, int abs){
  return argmax_d(V.vec, V.len, V.stride, abs);
}
int argmin_d(double *V, int len, int stride, int abs);
static int dvector_argmin(dvector V, int abs){
  return argmin_d(V.vec, V.len, V.stride, abs);
}
double dvec_sum(double *V, int len, int stride, int abs);

#ifndef OPENBLAS_VERSION
//Wrappers around cblas complex dot products which act like the
//the dot products for real numbers (i.e return a scalar)
static inline _Complex double cblas_zdotu(const int N, const void *X, const int incX,
                                         const void *Y, const int incY){
  complex_double dotu;
  cblas_zdotu_sub(N,X,incX,Y,incY,&dotu);
  return dotu;
}
static inline _Complex double cblas_zdotc(const int N, const void *X, const int incX,
                                         const void *Y, const int incY){
  complex_double dotc;
  cblas_zdotc_sub(N,X,incX,Y,incY,&dotc);
  return dotc;
}
#endif
#ifdef __cplusplus
}
#endif
#endif /* __LINALG_H__ */
