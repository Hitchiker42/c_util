#include "math_util.h"
#include "matrix.h"
double* identity_matrix_double(int n){
  double *mem = zmalloc(n * n * sizeof(double));
  int i;
  for(i=0;i<n;i++){
    mem[i*n + i] = 1;
  }
  return mem;
}
double* random_double_vector(int n){
  double *mem = xmalloc(n*sizeof(double));
  util_rand_state state = util_auto_rand_state();
  int i = 0;
  while(i<n){
    mem[i++] = util_drand_r(&state);
  }
  return mem;
}
double* double_matrix_copy(double *mat, int rows, int cols, int stride){
  double *mem = xmalloc(stride * cols * sizeof(double));
  if(stride == rows){
    memcpy(mem, mat, rows * cols * sizeof(double));
  } else {
    int i, stride = stride;
    for(i=0;i<rows;i++){
      memcpy(mem + i*stride, mat + i*stride, cols * sizeof(double));
    }
  }
  return mem;
}
static inline double __attribute__((always_inline))
_reduce(double *V, int len, int stride, double(*f)(double,double)){
  int i;
  double x = V[0];
  for(i=1;i<len;i++){
    x = f(x,V[i*stride]);
  }
  return x;
}
static inline int __attribute__((always_inline))
_indexed_reduce(double *V, int len, int stride, int(*f)(double*,int,int)){
  int i, idx = 0;
  for(i=1;i<len;i++){
    idx = f(V,idx, i*stride);
  }
  return idx;
}
static inline void __attribute__((always_inline))
_map_into(double *V, int len, int stride, double(*f)(double)){
  int i;
  for(i=0;i<len;i++){
    V[i*stride] = f(V[i*stride]);
  }
  return;
}
static int get_max(double *V, int i, int j){
  return (V[i] > V[j] ? i : j);
}
static int get_abs_max(double *V, int i, int j){
  return (fabs(V[i]) > fabs(V[j]) ? i : j);
}
static int get_min(double *V, int i, int j){
  return (V[i] <= V[j] ? i : j);
}
static int get_abs_min(double *V, int i, int j){
  return (fabs(V[i]) <= fabs(V[j]) ? i : j);
}
static double double_add(double x, double y){
  return x + y;
}
static double double_add_abs(double x, double y){
  return fabs(x) + fabs(y);
}
int argmax_d(double *V, int len, int stride, int abs){
  if(abs){
    return _indexed_reduce(V,len,stride,get_abs_max);
  } else {
    return _indexed_reduce(V,len,stride,get_max);
  }
}
int argmin_d(double *V, int len, int stride, int abs){
  if(abs){
    return _indexed_reduce(V,len,stride,get_abs_min);
  } else {
    return _indexed_reduce(V,len,stride,get_min);
  }
}
double dvec_sum(double *V, int len, int stride, int abs){
  if(abs){
    return _reduce(V,len,stride,double_add_abs);
  } else {
    return _reduce(V,len,stride,double_add);
  }
}

double reduce_d(double *V, int len, int stride, double(*f)(double,double)){
  return _reduce(V,len,stride,f);
}
