	.file	"matrix_util.c"
	.text
	.p2align 4,,15
	.globl	vector_argmin
	.type	vector_argmin, @function
vector_argmin:
.LFB17:
	.cfi_startproc
	testl	%ecx, %ecx
	jne	.L2
	cmpl	$1, %esi
	jle	.L9
	movslq	%edx, %r11
	movl	%edx, %r8d
	xorl	%eax, %eax
	salq	$3, %r11
	movl	$1, %ecx
	leaq	(%rdi,%r11), %r9
	.p2align 4,,10
	.p2align 3
.L8:
	movslq	%eax, %r10
	movsd	(%rdi,%r10,8), %xmm0
	ucomisd	(%r9), %xmm0
	cmovbe	%r8d, %eax
	addl	$1, %ecx
	addq	%r11, %r9
	addl	%edx, %r8d
	cmpl	%ecx, %esi
	jne	.L8
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1, %esi
	jle	.L9
	movslq	%edx, %r11
	movsd	.LC0(%rip), %xmm2
	salq	$3, %r11
	movl	%edx, %r8d
	xorl	%eax, %eax
	leaq	(%rdi,%r11), %r9
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L6:
	movslq	%eax, %r10
	movsd	(%r9), %xmm0
	movsd	(%rdi,%r10,8), %xmm1
	andpd	%xmm2, %xmm0
	andpd	%xmm2, %xmm1
	ucomisd	%xmm0, %xmm1
	cmovbe	%r8d, %eax
	addl	$1, %ecx
	addq	%r11, %r9
	addl	%edx, %r8d
	cmpl	%ecx, %esi
	jne	.L6
	rep ret
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE17:
	.size	vector_argmin, .-vector_argmin
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.ident	"GCC: (GNU) 6.2.1 20160830"
	.section	.note.GNU-stack,"",@progbits
