#include "math_common.h"
/*
  Various number theoritic functions related to prime numbers
*/
/*
  Prime counting function:
  π(x) = number of primes less than x
  π(x) ≈ (x/ln(x))(1 + (1/ln(x)) + (2/ln^2(x)) + O(3/ln^3(x)))
*/
//Upper bound for the number of primes less than n, useful for preallocating
//memory
ulong prime_count_upper_bound(ulong n){
  if(n > 60184){
    return (ulong)(ceil(x/(log(x)-1.1)));
  } else {
    return ((x/log(x)) * (1 +  1.2762/log(x)));
  }
}
//Not sure why you'd need this
ulong prime_count_lower_bound(ulong n){
  return (ulong)(n / log(n));
}
  
