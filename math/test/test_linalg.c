#include "../../C_util.h"
#include "../matrix.h"

//Returns 0 if each element of A and B are within max_ulp_diff ulps of each
//other, if the difference of two elements is greater than max_ulp_diff
//1+ the index of the mismatch is returned (i.e the 1 based index).
int double_vector_compare(int len, double *A, double *B, int max_ulp_diff){
  int i;
  for(i=0;i<len;i++){
    if(!double_ulp_compare(A[i],B[i],max_ulp_diff)){
      return i+1;
    }
  }
  return 0;
}
//compares two double matrices with a tolerance of 'max_ulp_diff'. If the
//i,jth elements differ by more than 'max_ulp_diff' ulps then (i*columns + j)+1
//is returned (one is added so that 0 can be returned for a true result).
//note that i is scaled by columns, since A and B may have different strides
int double_matrix_compare(int rows, int columns,
                          double *A, int A_stride,
                          double *B, int B_stride,
                          int max_ulp_diff){
  int i;
  for(i=0;i<rows;i++){
    int idx = double_vector_compare(columns, A + i*A_stride, B +i*B_stride,
                                    max_ulp_diff);
    if(idx){
      return (i*columns) + idx;
    }
  }
}      
