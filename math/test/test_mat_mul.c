#include "../../C_util.h"
#include "../mat_mul.h"
#include <cblas.h>
#include <dlfcn.h>

#if 0
//warning these macros has to be used in the same scope
//FTZ = bit 15, DAZ = bit 6
#define MXCSR_SET_DAZ_AND_FTZ(daz, \
int oldMXCSR__ = _mm_getcsr(); /*read the old MXCSR setting */ \
int newMXCSR__ = oldMXCSR__ | 0x8040; /* set DAZ and FZ bits */ \
_mm_setcsr( newMXCSR__ ); /*write the new MXCSR setting to the MXCSR */

#define MXCSR_RESET_DAZ_AND_FTZ \
/*restore old MXCSR settings to turn denormals back on if they were on*/ \
_mm_setcsr( oldMXCSR__ );
#endif
/*
  Compare A to B, A and B are considered equal if there is no element
  of A with more than max_ulps difference from the same element of B.
  If a difference is found the value returned is the linear index of the
  differing values (i.e for index i,j we return x = N*i + j). The maximum
  difference in ulps is stored in 'max_err_ptr' if it is not null.

  (this is a pretty generic function than could be used outside this file).
*/
int mat_approx_equal(size_t N, size_t M, int max_ulps,
                     double * restrict A, size_t A_stride,
                     double * restrict B, size_t B_stride,
                     int *max_err_ptr, size_t *err_loc){
  int i,j;
  int max_err = 0;
  for(i=0;i<M;i++){
    for(j=0;j<N;j++){
      int ulps = double_ulp_diff(A[i*A_stride + j], B[i*B_stride + j]);
      max_err = MAX(max_err, ulps);
      if(ulps > max_ulps){
        if(max_err_ptr){
          *max_err_ptr = max_err;
        }
        if(err_loc){
          *err_loc = (i*N + j);
        }
        return -1;
      }
    }
  }
  if(max_err_ptr){
    *max_err_ptr = max_err;
  }
  return 0;
}
int vec_approx_equal(size_t sz, int max_ulps,
                     double * restrict A,
                     double * restrict B){
  size_t i;
  for(i=0;i<sz;i++){
    int ulps = double_ulp_diff(A[i], B[i]);
    if(ulps > max_ulps){
      return -1;
    }
  }
  return 0;
}
int vec_max_err(size_t sz,
                double * restrict A,
                double * restrict B){
  size_t i;
  int max_err = 0;
  for(i=0;i<sz;i++){
    int ulps = double_ulp_diff(A[i], B[i]);
    max_err = MAX(max_err, ulps);
  }
  return max_err;
}
double vec_avg_err(size_t sz,
                double * restrict A,
                double * restrict B){
  size_t i;
  double total_err = 0;
  for(i=0;i<sz;i++){
    long ulps = double_ulp_diff(A[i], B[i]);
    double tmp = total_err;
    total_err += ((double)ulps/sz);
  }
  return total_err/sz;
}
//There's no need to pass stride, since the backing memory for each array
//is the same size as the array (when testing, that is)
struct mat_mul_args {
  size_t M;
  size_t N;
  size_t P;
  const double *restrict A;
  const double* restrict B;
  double* restrict C;
};
typedef void (*cblas_dgemm_t)(CBLAS_ORDER, CBLAS_TRANSPOSE,
                              CBLAS_TRANSPOSE, const int, const int,
                              const int, const double, const double *,
                              const int, const double *, const int,
                              const double, double *, const int);
typedef double* (*mat_mul_fn_t)(size_t, size_t, size_t,
                                const double* restrict, size_t,
                                const double* restrict, size_t,
                                double* restrict, size_t);
typedef void (*mat_mul_wrapper_fn_t)(struct mat_mul_args *, void *);

void mat_mul_blas(struct mat_mul_args *args, void *extra){
  int order = CblasRowMajor;
  int trans = CblasNoTrans;
  cblas_dgemm_t f = extra;
  //A is MxK, B is KxM, C is MxN (weird)
  //void cblas_dgemm(order, transA, transB, M, N, K, alpha,
  //                 A, lda, B, ldb, beta, C, ldc)
  f(order, trans, trans, //blas parameters
    args->M, args->P, args->N, //dimensions
    1, args->A, args->N, //A and a scalar multiplier
    args->B, args->P, 1, //B and a scalar multiplier
    args->C, args->P); //C
}
void mat_mul_wrapper(struct mat_mul_args *args, void *extra){
  mat_mul_fn_t f = extra;
  f(args->M, args->N, args->P,
    args->A, args->N,
    args->B, args->P,
    args->C, args->P);
}
/*
  Takes a set of arguments, a multiplication function, and a wrapper
  function which applies that function to thoes arguments and times
  how long it takes to do the multiplication. 'cnt' runs are done and
  stored in the array 'times'.

  The arrays should be large enough that the time taken to run the wrapper
  is negligable compared to the time required to do the multiplication.

  An expected result and maximum error are given, and after each multiplication
  the result is checked and if the difference between the result and the
  expected result is greater than the maximum error then the number of successful
  runs + 1 is returned. If there is no error in any of the runs 0 is returned.
*/
int time_mat_mul(struct mat_mul_args *args,
                 mat_mul_wrapper_fn_t wrapper,
                 void *fn,
                 int cnt, double *times,
                 double *expected_result,
                 int max_ulps_err, int *max_err){
  //size of the result matrix
  int M = args->M, N = args->P;
  double *C = args->C;
  size_t sz = M*N;
  ssize_t i,j;
  double start, end;
  for(j=0;j<cnt;j++){
    //zero out the result array
    memset(C,'\0',sz*sizeof(double));
    start = float_cputime();
    //    DEBUG_PRINTF("start time: %f\n", start);
    wrapper(args, fn);
    end = float_cputime();
    //    DEBUG_PRINTF("end time: %f\n", end);
    times[j] = (end-start);
    //    DEBUG_PRINTF("Total time: %f\n",times[j]);
    int err = mat_approx_equal(M, N, max_ulps_err,
                               expected_result, N,
                               args->C, N,
                               max_err, NULL);
    if(err){
      return j+1;
    }
  }
  return 0;
}
double *gen_rand_array(size_t sz){
  static const double int_to_float_multiplier =  (1.0/18446744073709551616.0);
  double *arr = xmalloc(sz*sizeof(double));
  int i;
  uint64_t __attribute__((aligned(16))) tmp[4];
  for(i=0;i<sz;i+=4){
    tmp[0] = util_rand();
    tmp[1] = util_rand();
    tmp[2] = util_rand();
    tmp[3] = util_rand();
    arr[i] = tmp[0]*int_to_float_multiplier;
    arr[i+1] = tmp[1]*int_to_float_multiplier;
    arr[i+2] = tmp[2]*int_to_float_multiplier;
    arr[i+3] = tmp[3]*int_to_float_multiplier;
  }
  return arr;
}
//We need to link a cblas lib into the executable, these are
//alternate blas implementations to test.
const char *blas_libs[] = {};
size_t num_blas_libs = sizeof(blas_libs);
/*
  Notes about array size,
  a 1024x1024 array takes 8Mb, //1024 = (1<<10)
  a 2048x2048 array takes 32Mb, //2048 = (1<<11)
  a 4096x4096 array takes 128Mb, and //4096 = (1<<12)
  a 8192x8192 array takes 512Mb. //8192 = (1<<13)
*/
const size_t A_rows = (1<<11);
const size_t A_cols = (1<<11);
const size_t B_rows = (1<<11);
const size_t B_cols = (1<<11);

#define TEST_COUNT 10
#define MAX_ULPS 100
static double average(double *arr, size_t len){
  int i;
  double avg = 0.0;
  for(i=0;i<len;i++){
    avg += arr[i]/len;
  }
  return avg;
}
static double minimum(double *arr, size_t len){
  double min = arr[0];
  int i;
  for(i=1;i<len;i++){
    min = MIN(min, arr[i]);
  }
  return min;
}
static double maximum(double *arr, size_t len){
  double max = arr[0];
  int i;
  for(i=1;i<len;i++){
    max = MAX(max, arr[i]);
  }
  return max;
}
int main(int argc, char *argv[]){
  int test_count = TEST_COUNT;
  int max_ulps = MAX_ULPS;
  //use the first argument as the test count, if it parses as a number
  if(argc >= 1){
    long tmp = strtol(argv[0], NULL, 0);
    if(tmp){
      test_count = tmp;
    }
  }
  /*   cblas_dgemm_t blas_functions[num_blas_libs] = {NULL}; */
  /* int i; */
  /* for(i=0;i<num_blas_libs;i++){ */
  /*   void *handle = dlopen(blas_libs[i], RTLD_LAZY | RTLD_LOCAL); */
  /*   blas_functions[i] = dlsym(handle, "cblas_dgemm"); */
  /* } */
  //  enable_backtraces();
  int i;
  ssize_t M = A_rows, N = A_cols, P = B_cols;
  DEBUG_PRINTF("M = %ld, N = %ld, P = %ld\n",M,N,P);
  util_srand_auto();
  DEBUG_PRINTF("Generating arrays\n");
  double *restrict A = gen_rand_array(M*N);
  double *restrict B = gen_rand_array(N*P);

  double *restrict C = zmalloc(M*P * sizeof(double));
  double *restrict control = zmalloc(M*P * sizeof(double));
  DEBUG_PRINTF("Creating control\n");
  control = mat_mul_recursive(M, N, P, //dims
                              A, N,//A, A_stride
                              B, P,//B, B_stride
                              control, P);//C, C_stride
  struct mat_mul_args args = {M,N,P,A,B,C};
  double *times = malloc(test_count*sizeof(double));
  int err = 0, max_err;
#define DO_TEST(wrapper, fn)                                            \
  DEBUG_PRINTF("Timing "#fn"\n");                                       \
  printf("Timing "#fn"\n");                                             \
  err = time_mat_mul(&args, wrapper, fn, test_count,                    \
                     times, control, max_ulps, &max_err);               \
  if(err){                                                              \
    fprintf(stderr, "Error in "#fn"\n"                                  \
            "Maximum allowed error = %d, actual maximum error = %d\n",  \
            max_ulps, max_err);                                         \
    exit(1);                                                            \
  }                                                                     \
  printf("Avg time for "#fn" = %f\n", average(times, test_count));      \
  printf("Best: %f\nWorst: %f\n", minimum(times, test_count),           \
         maximum(times, test_count));                                   \
  printf("Individual times: %f", times[0]);                             \
  for(i=1;i<test_count;i++){                                            \
    printf(", %f",times[i]);                                            \
  }                                                                     \
  printf("\nMaximum error: %d\n", max_err);                             \
  fflush(stdout);

  DO_TEST(mat_mul_wrapper, mat_mul_recursive);
//This is about 10x slower than the recursive version
//  DO_TEST(mat_mul_wrapper, mat_mul_naive);
//  DO_TEST(mat_mul_blas, cblas_dgemm);

  return 0;
}
