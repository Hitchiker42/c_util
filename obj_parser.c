#include "C_util.h"
#define STRING_BUF_MACROS
#include "string_buf.h"
/*
  I'm assuming that there can be only one statement per line, and what that
  statement does is determinied by the first character(s) of the line.
  I can't find anywhere that definitively states this, but it's implied
  in a lot of places and all examples I've seen follow this convention.
*/
/*
  //I'm not supporting any of the free-form geometry commands
  Commands:
  Supported:
    -v, vertex position
    -vt, vertex texture coordinates
    -vn, vertex normal
    -p, point element
    -l, line element
    -f, polygonal face element
    -#, comment
    -o, object name, optional so I can safely ignore it 
  May support later:
    -g, grouping
    -s, smoothing group (used for interpolation)
    -usemtl, material name
    -mtlib, material library
  Unsupported:
    -vp, deg, bmat, step: free form vertex data
    -curv, curv2, surf: free form elements
    -parm, trim, hole, scrv, sp, end, con: free form specific commands
    -mg: free form group command
    -bevel, c_interp, d_interp, lod, shadow_obj, trace_obj, ctech, stech

*/
//I'm sure this could be more efficent, but it should be faster than using sscanf
static inline int parse_vertex(char *str, size_t len,
                               float *buf, int min, int max, float def){
  int i;
  char *endptr;
  for(i=0;i<max;i++){
    errno = 0;
    buf[i] = strtof(str, &endptr);
    if(errno != 0){
      return -1;
    }
    if(str == endptr){
      break;
    }
  }
  if(i < min){
    return -1;
  } else {
    for(;i<max;i++){
      buf[i] = def;
    }
  }
  return 0;
}
//It would probably make sense to allocate a bunch of memory at the start
//and use that pool rather than allocating each vertex attribute seperately
gl_mesh *parse_obj_file(char *filename){
  size_t sz;
  uint8_t *file = mmap_filename_read_only(filename, &sz);
  svector positions = make_svector(16);
  svector normals = make_svector(16);
  svector tex_coords = make_svector(16);
  svector faces = make_svector(16);
  uint8_t *buf, *line_end;
  buf = file;
  size_t buf_sz = sz;
  int line_no = 0;
  while((line_end = memchr(buf, '\n', buf_sz))){
    buf_sz -= (line_end-buf);
    *line_end = '\0';//to make parsing eaiser
    switch(buf[0]){
      case 'v': {//vertex data
        switch(buf[1]){
          case ' ': {//position vector
            float *pos = xmalloc(4*sizeof(float));
            int err = parse_vertex(buf+2, line_end - (buf+2), pos, 3, 4, 1.0f);
            if(err){goto error;}
            SVECTOR_PUSH(pos, positions);
            continue;
          }
          case 'n': {//normal vector
            float *norm = xmalloc(3*sizeof(float));
            int err = parse_vertex(buf+2, line_end - (buf+2), pos, 3, 3, 0.0f);
            if(err){goto error;}
            SVECTOR_PUSH(norm, normals);
            continue;
          }
          case 't': {//texture coordinate
            float *texc = xmalloc(3*sizeof(float));
            int err = parse_vertex(buf+2, line_end - (buf+2), pos, 1, 3, 0.0f);
            if(err){goto error;}
            SVECTOR_PUSH(texc, tex_coords);
            continue;
          }
          case 'p': {//point in paramater space of a curve/surface
            fprintf(stderr, "Free form geometry unsupported:\nLine %d: %.*s",
                    line_no, (line_end-buf), buf);
            goto error;
          }
          default: {
            fprintf(stderr, "Undefined command:\nLine %d: %.*s",
                    line_no, (line_end-buf), buf);
            goto error;
          }
        }
      }           
      case 'p': {//point
      }
      case 'l': {//line
      }
      case 'f': {//face
      }
      case 'g': {//group
      }
      case 's': {//smoothing
      }
      //unsupported commands
      case 'u': {//usemtl (use material)
      }
      case 'm': {//mtlib (load a material library)
      }
      case 'c': {//bunch of commands
      }
        
      case 'o'://optional (we ignore it)
      case '#': {//comment
        buf = line_end;
        line_no++;
        continue;
      }
        
