#ifndef _SVECTOR_H_
#define _SVECTOR_H_
/*
  Simple dynamic arrays, similar to std::vector in C++.

  There are multiple versions of every function/macro.
  lowercase functions take svectors as pointers, upper case ones are
  actually macros and take literals. Macros ending in _GEN take a type
  parameter which specifies that the svector should be interpreted as
  being of the given type.
*/
#if (defined __LIBCUTIL__)
#include "C_util.h"
#else
/*
  This file and svector.c can be used without the rest of the
  library if necessary.
*/
#include <stdlib.h>
#include <string.h>
#define MIN(_x,_y)                              \
  __extension__                                 \
  ({__typeof(_x) x = _x;                        \
    __typeof(_y) y = _y;                        \
    x<y ? x : y;})
#define MAX(_x,_y)                              \
  __extension__                                 \
  ({__typeof(_x) x = _x;                        \
    __typeof(_y) y = _y;                        \
    x>y ? x : y;})
#endif

#ifndef XMALLOC
#define xmalloc malloc
#endif
#ifndef XREALLOC
#define xrealloc realloc
#endif

typedef struct svector svector;
struct svector {
  union {
    void **data;
    uint8_t *bytes;
    uint16_t *shorts;
    uint32_t *ints;
    uint64_t *longs;
    double *doubles;
    float *floats;
  };
  int len;
  int size;
};
#ifndef SVECTOR_DEFAULT_SIZE
#define SVECTOR_DEFAULT_SIZE 8
#endif
/*
  Make sure the size of the svector is at least size*type_size bytes.
  The current size is interpreted as being in type_size units.
*/
static inline void svector_ensure_size(svector *svec, int size, int type_size){
  int needed = size * type_size;
  if(svec->size*type_size < needed){
      int new_size = MAX(MAX(svec->size*2,SVECTOR_DEFAULT_SIZE), size);
      svec->size = new_size;
      svec->data = xrealloc(svec->data, svec->size*type_size);
  }
}
//Ensures svec can hold sz elements of the given type
#define svector_reserve_gen(svec,sz,type)       \
  svector_ensure_size(svec,sz,sizeof(type))
#define svector_reserve(svec,sz)                        \
  svector_ensure_size(svec,sz,sizeof(void*))

#define SVECTOR_RESERVE_GEN(svec,sz,type)               \
  svector_ensure_size(&svec,sz,sizeof(type))
#define SVECTOR_RESERVE(svec,sz)                        \
  svector_ensure_size(&svec,sz,sizeof(void*))

//check that the svector can hold cnt more elements of the given type
//should probably be renamed, but I use it a bunch and it'd be
//annoying to change.
#define svector_check_size_gen(svec,cnt,type)                   \
  svector_ensure_size(svec, (svec)->len+cnt, sizeof(type))
#define svector_check_size(svec,sz) svector_check_size_gen(svec,sz,void*)


#define svector_data_gen(x,type) ((type*)(x->data))
#define svector_data(x) (x->data)
#define svector_len(x) (x->len)

#define SVECTOR_DATA_GEN(x,type) ((type*)(x.data))
#define SVECTOR_DATA(x) (x.data)
#define SVECTOR_LEN(x) (x.len)
#define SVECTOR_LENGTH(x) (x.len)

static struct svector make_svector_gen(int size, int elt_size){
  svector ret = {.size = (size || SVECTOR_DEFAULT_SIZE), .len = 0};
  ret.data = malloc(size*elt_size);
  if(ret.data == 0){
    perror("malloc");
    raise(SIGABRT);
  }
  return ret;
}
static struct svector make_svector(int size){
  return make_svector_gen(size, sizeof(void*));
}
#define SVECTOR_POP_GEN(vec,type) ((type*)(vec.data))[--vec.len]
#define SVECTOR_POP(vec) (vec.data[--vec.len])
static inline void *svector_pop(struct svector *vec){
  return vec->data[--vec->len];
}

#define SVECTOR_PUSH(elt, vec)                  \
  svector_check_size(&vec, 1);                  \
  vec.data[vec.len++] = elt
#define SVECTOR_PUSH_GEN(elt, vec, type)        \
  svector_check_size_gen(&vec, 1, type);        \
  ((type*)vec.data)[vec.len++] = elt
static inline void svector_push(void *elt, struct svector* vec){
  svector_check_size(vec,1);
  vec->data[vec->len++] = elt;
}
//is an lvalue
#define SVECTOR_REF(vec, idx) (vec.data[idx])
#define SVECTOR_REF_GEN(vec, idx, type) (((type*)vec.data)[idx])
static inline void* svector_ref(const struct svector *vec, int idx){
  return vec->data[idx];
}
#define SVECTOR_SET(vec, idx, elt) (vec.data[idx] = elt)
#define SVECTOR_SET_GEN(vec, idx, elt, type) (((type*)vec.data)[idx] = elt)
static inline void svector_set(struct svector *vec, int idx, void *elt){
  vec->data[idx] = elt;
}

#define SVECTOR_SWAP_GEN(vec, i, j, type)                               \
  __extension__                                                         \
  ({type __tmp1 = SVECTOR_REF_GEN(vec,i,type);                          \
    type __tmp2 = SVECTOR_REF_GEN(vec,j,type);                          \
    SVECTOR_SET_GEN(vec, j, __tmp1, type);                              \
    SVECTOR_SET_GEN(vec, i, __tmp2, type);})

static inline void svector_swap(struct svector *vec, int i, int j){
  void *temp = vec->data[i];
  vec->data[i] = vec->data[j];
  vec->data[j] = temp;
}

struct svector copy_svector(const struct svector *svec);
struct svector init_svector(int size, int len, const void* data);
struct svector svector_reverse(const struct svector *svec);
struct svector svector_reverse_inplace(struct svector *svec);
int svector_find(const struct svector *svec, void *elt);
int svector_search(const struct svector *svec, int(*test)(void*));
//search with user provided data passed as second argument
int svector_search2(const struct svector *svec,
                    int(*test)(void*,void*), void *data);
struct svector svector_sort(struct svector *svec, int(*cmp)(void*,void*),
                            int stable, int inplace);
/*struct svector svector_sort_inplace(struct svector *svec,
                                    int(*cmp)(void*,void*));*/
//these are less useful than they would be in a langage with lambdas
void* svector_reduce(const struct svector *vec, void*(*f)(void*,void*));
struct svector svector_map(const struct svector *vec, void*(*f)(void*));
struct svector svector_map_inplace(struct svector *vec, void*(*f)(void*));
//apply f to each element of vec for side effects only
void svector_mapc(const struct svector *vec, void(*f)(void*));
//Free the data associated with svec, svec itself is not freed, since
//more often than not it's on the stack
void destroy_svector(struct svector *svec);
#endif
