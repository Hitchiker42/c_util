#include "C_util.h"

int test_bitvector_init(int sz){
  struct bitvector bv = make_bitvector(sz, 0);
  int i;
  for(i=0;i<(sz/8);i++){
    assert(bv.bytes[i] == 0);
  }
  free_bitvector(bv);
  bv = make_bitvector(sz, 1);
  for(i=0;i<(sz/8);i++){
    assert(bv.bytes[i] == 0xff);
  }
  return 0;
}
int test_bitvector_set(){
  struct bitvector bv = make_bitvector(64,0);
  int i;
  for(i=0;i<8;i++){
    bitvector_set(bv,(i*8 + i));
  }
  for(i=0;i<8;i++){
    assert(bv.bytes[i] == (1<<i));
  }
  free_bitvector(bv);
  bv = make_bitvector(64,0);
  for(i=0;i<8;i++){
    bitvector_set(bv,(i*8 + (7-i)));
  }
  for(i=0;i<8;i++){
    assert(bv.bytes[i] == (1<<(7-i)));
  }
  free_bitvector(bv);
  return 0;
}
int test_bitvector_unset(){
  struct bitvector bv = make_bitvector(64,1);
  int i;
  for(i=0;i<8;i++){
    bitvector_unset(bv,(i*8 + i));
  }
  for(i=0;i<8;i++){
    //    fprintf(stderr,"bv.bytes[%d] = %hhx (should be %hhx)\n",i,bv.bytes[i],~(1<<i));
    assert(bv.bytes[i] == (uint8_t)(~(1<<i)));
  }
  free_bitvector(bv);
  bv = make_bitvector(64,1);
  for(i=0;i<8;i++){
    bitvector_unset(bv,(i*8 + (7-i)));
  }
  for(i=0;i<8;i++){
    assert(bv.bytes[i] == (uint8_t)~(1<<(7-i)));
  }
  free_bitvector(bv);
  return 0;
}
int main(){
  assert(test_bitvector_init(6400) == 0);
  assert(test_bitvector_set() == 0);
  assert(test_bitvector_unset() == 0);
}
