#include "../C_util.h"
int main(int argc, char *argv[]){
  if(argc < 3){
    fprintf(stderr,"Need 2 arguments\n");
    exit(1);
  }
  double x = strtod(argv[1],NULL);
  double y = strtod(argv[2],NULL);
  fprintf(stderr, "err = %ld\n", double_ulp_diff(x,y));
}
