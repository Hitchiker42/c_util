#include "C_util.h"
#define COUNT (1<<20)
int test_svector_growth(int count){
  svector svec = make_svector(0); //deliberately use 0 here
  int i;
  for(i=0;i<count;i++){
    SVECTOR_PUSH(i, svec);
  }
  for(i=0;i<count;i++){
    assert(SVECTOR_REF(svec,i) == i);
  }
  return 1;
}
int test_svector_ptr_growth(int count){
  svector *svec = xmalloc(sizeof(svector));
  *svec = make_svector(0);
  int i;
  for(i=0;i<count;i++){
    svector_push(i, svec);
  }
  for(i=0;i<count;i++){
    assert(svector_ref(svec,i) == i);
  }
  return 1;
}

int main(){
  assert(test_svector_growth(COUNT) == 1);
  assert(test_svector_ptr_growth(COUNT) == 1);
}
